package cursoemvideo.meupainel.pages.cursosexclusivos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CursosExclusivosElements {

    @FindBy(xpath = "(//div[@class='uabb-infobox-left-right-wrap']//a[@class='uabb-infobox-module-link'])[7]")
    public WebElement cmpCursosExclusivos;

    @FindBy(xpath = "//span[@class='ld-course-status-price']")
    public WebElement situacaoCurso;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/recados-apoiadores-vip/']")
    public List<WebElement> recadosImportantes;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/como-anunciar-na-internet-vip/']")
    public List<WebElement> cursoComoAnunciarNaInternet;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/seguranca-da-informacao-modulo-01-vip/']")
    public List<WebElement> cursoSegurancaInformacaoMod1;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/seguranca-da-informacao-modulo-02-vip/']")
    public List<WebElement> cursoSegurancaInformacaoMod2;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/seguranca-da-informacao-modulo-0-vip/']")
    public List<WebElement> cursoSegurancaInformacaoMod0;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/seguranca-da-informacao-modulo-03-vip/']")
    public List<WebElement> cursoSegurancaInformacaoMod3;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/seguranca-da-informacao-modulo-4-vip/']")
    public List<WebElement> cursoSegurancaInformacaoMod4;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/wordpress-modulo-4-vip/']")
    public List<WebElement> cursoWordpressMod4;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-de-php-moderno-modulo-1-vip/']")
    public List<WebElement> cursoPhpModernoMod1;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-pratico-de-seo-modulo-01-vip/']")
    public List<WebElement> cursoPraticoCEOMod1;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-pratico-de-seo-modulo-02-vip/']")
    public List<WebElement> cursoPraticoCEOMod2;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-de-ingles-para-iniciantes-modulo-1-beginners-completo-vip/']")
    public List<WebElement> cursoInglesInicMod1Beg;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-de-ingles-para-iniciantes-modulo-2-beginners-completo-vip/']")
    public List<WebElement> cursoInglesInicMod2Beg;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-de-ingles-para-iniciantes-modulo-3-beginners-completo-vip/']")
    public List<WebElement> cursoInglesInicMod3Beg;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/html5-css3-modulo1-vip/']")
    public List<WebElement> cursoIHtmlCssMod1;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/html5-css3-modulo-2-vip/']")
    public List<WebElement> cursoIHtmlCssMod2;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/html5-e-css3-modulo-3-de-5-vip/']")
    public List<WebElement> cursoIHtmlCssMod3;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/html5-e-css3-modulo-4-de-5-vip/']")
    public List<WebElement> cursoIHtmlCssMod4;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/html5-e-css3-modulo-5-de-5-vip/']")
    public List<WebElement> cursoIHtmlCssMod5;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/bastidores-cursoemvideo/']")
    public List<WebElement> bastidoresCursoEmVideo;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/algoritmos-portugol-studio-vip/']")
    public List<WebElement> algoritmosPortugolStudio;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/curso/curso-em-video-experience/']")
    public List<WebElement> cursoEmVideoExperience;

}
