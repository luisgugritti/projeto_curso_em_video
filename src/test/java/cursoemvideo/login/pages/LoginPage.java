package cursoemvideo.login.pages;

import cursoemvideo.dto.CadastroDTO;
import cursoemvideo.dto.LoginDTO;
import cursoemvideo.base.BasePage;
import cursoemvideo.utils.Urls;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    protected WebDriver driver;
    private LoginElements loginElements;

    public LoginPage() {
        this.driver = BasePage.getDriver();
        loginElements = new LoginElements();
        PageFactory.initElements(driver, loginElements);
    }

    public void acessarTelaDeLogin() throws Exception {
        acessarUrl(Urls.urlLogin);
        esperarElemento(loginElements.btnAceitaCookie);
        clicarBotao(loginElements.btnAceitaCookie);
    }

    public void acessarTelaDeLoginSemCookie() throws Exception {
        acessarUrl(Urls.urlLogin);
    }

    public void realizarLogin(LoginDTO dto) throws Exception {
        if (dto.getLogin() != null) {
            escrever(loginElements.cmpEmail, dto.getLogin());
        }

        if (dto.getSenha() != null) {
            escrever(loginElements.cmpSenha, dto.getSenha());
        }

        clicarBotao(loginElements.btnLogin);
    }

    public void validarLoginDeSucesso() throws Exception {
        esperarUrl(Urls.urlPagInicial);

        Assert.assertTrue("Validando login com sucesso",
                Urls.urlPagInicial.contains("https://www.cursoemvideo.com/minha-conta/"));
    }

    public void validarMsgErroSenhaIncorreta() throws Exception {
        esperarElemento(loginElements.msgErroSenhaIncorreta);

        Assert.assertTrue("Validando mensagem de erro por senha incorreta",
                loginElements.msgErroSenhaIncorreta.getText().trim().contains("Error: The Password you have entered is Invalid."));
    }

    public void validarMsgErroEmailInvalido() throws Exception {
        esperarElemento(loginElements.msgErroEmailInvalido);

        Assert.assertTrue("Validando mensagem de erro por email invalido",
                loginElements.msgErroEmailInvalido.getText().trim().contains("Error: The Username you have entered is Invalid"));
    }

    public void preencherCampoEmail(String email) {
        esperarElemento(loginElements.cmpEmail);
        escrever(loginElements.cmpEmail, email);
    }

    public void preencherCampoSenha(String senha) {
        esperarElemento(loginElements.cmpSenha);
        escrever(loginElements.cmpSenha, senha);
    }

    public void clicarBotaoLogin() {
        clicarBotao(loginElements.btnLogin);
    }

    public void facoLoginComOsDadosDoCadastro() {
        CadastroDTO cadastroDTO = BasePage.getCadastroDTO();
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setLogin(cadastroDTO.getEmail());
        loginDTO.setSenha(cadastroDTO.getSenha());

        preencherCampoEmail(loginDTO.getLogin());
        preencherCampoSenha(loginDTO.getSenha());
        clicarBotaoLogin();
    }

}