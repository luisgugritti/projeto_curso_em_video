package cursoemvideo.meupainel.steps;

import cursoemvideo.meupainel.pages.ajuda.AjudaPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class AjudaSteps {

    private AjudaPage ajudaPage;

    @Before
    public void open() {
        ajudaPage = new AjudaPage();
    }

    @And("^seleciono a duvida (.*)$")
    public void selecionarDuvida(String duvida) throws Exception {
        ajudaPage.selecionarDuvida(duvida);
    }

    @Then("^verifico se o conteudo do campo (.*) esta correto$")
    public void verificarContCmp(String campo) throws Exception {
        ajudaPage.verificarContCmp(campo);
    }

    @And("^seleciono o assunto (.*)$")
    public void selecionarAssunto(String assunto) throws Exception {
        ajudaPage.selecionarAssunto(assunto);
    }

    @And("^preencho o campo obrigatorio Mensagem$")
    public void preencherCmpMsg() throws Exception {
        ajudaPage.preencherCmpMsg();
    }

    @And("^clico no botao Enviar$")
    public void clicarBtnEnviar() throws Exception {
        ajudaPage.clicarBtnEnviar();
    }

    @Then("^valida mensagem de (.*)$")
    public void validarTipoMsg(String msg) throws Exception {
        ajudaPage.validarTipoMsg(msg);
    }

}
