package cursoemvideo.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = false,
        stepNotifications = false,
        useFileNameCompatibleName = false,
        features = "src/test/resources/features",
        glue = "cursoemvideo",
        plugin = {
                "pretty",
                "junit:target/surefire-reports/result.xml",
                "html:target/cucumber-reports"
        },
        tags = "@Apoie and @CT002"
)
public class Runner {
}
