@Apoie
  Feature: Tela Apoio

    Background: Apoiar a plataforma financeiramente

      @CT001
      Scenario: Realizar Assinatura Mensal
        Given acessar tela de cadastro
        When preencho os campos de cadastro de forma automatica
        And clicar no botao Registrar Gratuitamente
        And acessar tela de login sem cookie
        Then realizar login com os dados do cadastro
        When seleciona a opcao Assinatura
        And escolher apoio mensal
        And clica em Assine Agora
        When na tela Finalizar compra preenche os campos de Detalhes de faturamento de forma automatica
        And selecionar o tipo de pagamento cartao de credito
        And selecionar e concordar com os termos e condicoes do site e clicar em assine agora
        Then valido mensagem de erro cpf invalido

      @CT002
      Scenario: Realizar Assinatura Anual
        Given acessar tela de cadastro
        When preencho os campos de cadastro de forma automatica
        And clicar no botao Registrar Gratuitamente
        And acessar tela de login sem cookie
        Then realizar login com os dados do cadastro
        When seleciona a opcao Assinatura
        And escolher apoio anual
        And clica em Assine Agora
        When na tela Finalizar compra preenche os campos de Detalhes de faturamento de forma automatica
        And selecionar o tipo de pagamento cartao de credito
        And selecionar e concordar com os termos e condicoes do site e clicar em assine agora
        Then valido mensagem de erro cartao nao aprovado