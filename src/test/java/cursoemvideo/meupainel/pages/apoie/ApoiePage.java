package cursoemvideo.meupainel.pages.apoie;

import cursoemvideo.base.BasePage;
import cursoemvideo.dto.ApoieDTO;
import cursoemvideo.utils.Variaveis;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;

public class ApoiePage extends BasePage {

    WebDriver driver;
    ApoieElements apoieElements;
    Variaveis variaveis;

    public ApoiePage() {
        this.driver = BasePage.getDriver();
        apoieElements = new ApoieElements();
        PageFactory.initElements(driver, apoieElements);
    }

    public void selecionarCampoQueroSerApoiador() throws Exception {
        esperarElemento(apoieElements.btnQueroSerApoiador.get(0));
        clicarBotao(apoieElements.btnQueroSerApoiador.get(0));
    }

    public void escolherApoio(String tipoApoio) throws Exception {

        switch (tipoApoio) {
            case "mensal":
                esperarElemento(apoieElements.btnApoieAgoraMensal);
                moverElemento(apoieElements.btnApoieAgoraMensal);
                clicarBotao(apoieElements.btnApoieAgoraMensal);
                break;
            case "anual":
                esperarElemento(apoieElements.btnApoieAgoraAnual);
                moverElemento(apoieElements.btnApoieAgoraMensal);
                clicarBotao(apoieElements.btnApoieAgoraAnual);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void clicaEmAssineAgora() throws Exception {
        esperarElemento(apoieElements.btnAssineAgora);
        moverElemento(apoieElements.btnAssineAgora);
        clicarBotao(apoieElements.btnAssineAgora);
    }

    public void preencherCamposFinalizarCompraPessoaFisica(ApoieDTO dto) throws Exception {

        if (dto.getNome() != null) {
            esperarElemento(apoieElements.cmpNome);
            moverElemento(apoieElements.cmpNome);
            escrever(apoieElements.cmpNome, dto.getNome());
        }
        if (dto.getSobrenome() != null) {
            esperarElemento(apoieElements.cmpSobrenome);
            moverElemento(apoieElements.cmpSobrenome);
            escrever(apoieElements.cmpSobrenome, dto.getSobrenome());
        }
        if (dto.getTipoPessoa() != null) {
            esperarElemento(apoieElements.cmpSelect.get(0));
            clicarBotao(apoieElements.cmpSelect.get(0));
            esperarElemento(apoieElements.inputTxtTipoPessoa);
            escreverEntrar(apoieElements.inputTxtTipoPessoa, dto.getTipoPessoa());
        }
        if (dto.getCpf() != null) {
            esperarElemento(apoieElements.cmpCpf);
            moverElemento(apoieElements.cmpCpf);
            escrever(apoieElements.cmpCpf, dto.getCpf());
        }
        if (dto.getDataNasc() != null) {
            esperarElemento(apoieElements.cmpDataNasc);
            moverElemento(apoieElements.cmpDataNasc);
            escrever(apoieElements.cmpDataNasc, dto.getDataNasc());
        }
        if (dto.getGenero() != null) {
            esperarElemento(apoieElements.cmpSelect.get(1));
            clicarBotao(apoieElements.cmpSelect.get(1));
            escreverEntrar(apoieElements.inputTxtGenero, determinarGenero(dto.getNome()));
        }
        if (dto.getPais() != null) {
            esperarElemento(apoieElements.cmpSelect.get(2));
            moverElemento(apoieElements.cmpSelect.get(2));
            clicarBotao(apoieElements.cmpSelect.get(2));
            escreverEntrar(apoieElements.inputTxtPais, dto.getPais());
        }
        if (dto.getCep() != null) {
            esperarElemento(apoieElements.cmpCep);
            moverElemento(apoieElements.cmpCep);
            escrever(apoieElements.cmpCep, dto.getCep());
        }
        if (dto.getEstado() != null) {
            esperarElemento(apoieElements.cmpSelect.get(3));
            moverElemento(apoieElements.cmpSelect.get(3));
            clicarBotao(apoieElements.cmpSelect.get(3));
            escreverEntrar(apoieElements.inputTxtEstado, dto.getEstado());
        }
        if (dto.getCidade() != null) {
            esperarElemento(apoieElements.cmpCidade);
            moverElemento(apoieElements.cmpCidade);
            escrever(apoieElements.cmpCidade, dto.getCidade());
        }
        if (dto.getNumero() != null ) {
            esperarElemento(apoieElements.cmpNumero);
            moverElemento(apoieElements.cmpNumero);
            escrever(apoieElements.cmpNumero, dto.getNumero());
        }
        if (dto.getEndereco() != null ) {
            esperarElemento(apoieElements.cmpEndereco);
            moverElemento(apoieElements.cmpEndereco);
            escrever(apoieElements.cmpEndereco, dto.getEndereco());
        }
        if (dto.getAdc() != null) {
            esperarElemento(apoieElements.cmpAdc);
            moverElemento(apoieElements.cmpAdc);
            escrever(apoieElements.cmpAdc, dto.getAdc());
        }
        if (dto.getBairro() != null) {
            esperarElemento(apoieElements.cmpBairro);
            moverElemento(apoieElements.cmpBairro);
            escrever(apoieElements.cmpBairro, dto.getBairro());
        }
        if (dto.getTelefone() != null) {
            esperarElemento(apoieElements.cmpTelefone);
            moverElemento(apoieElements.cmpTelefone);
            escrever(apoieElements.cmpTelefone, dto.getTelefone());
        }
        if (dto.getEmail() != null) {
            esperarElemento(apoieElements.cmpEmail);
            moverElemento(apoieElements.cmpEmail);
            limparCampo(apoieElements.cmpEmail);
            escrever(apoieElements.cmpEmail, dto.getEmail());
        }
        if (dto.getNotas() != null) {
            esperarElemento(apoieElements.cmpNotas);
            moverElemento(apoieElements.cmpNotas);
            escrever(apoieElements.cmpNotas, dto.getNotas());
        }

    }

    public void preencherCamposFinalizarCompraPessoaFisicaAutomatico(ApoieDTO dto) throws Exception {

        if (dto.getNome() != null) {
            esperarElemento(apoieElements.cmpNome);
            moverElemento(apoieElements.cmpNome);
            escrever(apoieElements.cmpNome, dto.getNome());
        }
        if (dto.getSobrenome() != null) {
            esperarElemento(apoieElements.cmpSobrenome);
            moverElemento(apoieElements.cmpSobrenome);
            escrever(apoieElements.cmpSobrenome, dto.getSobrenome());
        }
        if (dto.getTipoPessoa() != null) {
            esperarElemento(apoieElements.cmpSelect.get(0));
            clicarBotao(apoieElements.cmpSelect.get(0));
            esperarElemento(apoieElements.inputTxtTipoPessoa);
            escreverEntrar(apoieElements.inputTxtTipoPessoa, dto.getTipoPessoa());
        }
        if (dto.getCpf() != null) {
            esperarElemento(apoieElements.cmpCpf);
            moverElemento(apoieElements.cmpCpf);
            escrever(apoieElements.cmpCpf, dto.getCpf());
        }
        if (dto.getDataNasc() != null) {
            esperarElemento(apoieElements.cmpDataNasc);
            moverElemento(apoieElements.cmpDataNasc);
            escrever(apoieElements.cmpDataNasc, dto.getDataNasc());
        }
        if (dto.getGenero() != null) {
            esperarElemento(apoieElements.cmpSelect.get(1));
            clicarBotao(apoieElements.cmpSelect.get(1));
            escreverEntrar(apoieElements.inputTxtGenero, determinarGenero(dto.getNome()));
        }
        if (dto.getPais() != null) {
            esperarElemento(apoieElements.cmpSelect.get(2));
            moverElemento(apoieElements.cmpSelect.get(2));
            clicarBotao(apoieElements.cmpSelect.get(2));
            escreverEntrar(apoieElements.inputTxtPais, dto.getPais());
        }
        if (dto.getCep() != null) {
            esperarElemento(apoieElements.cmpCep);
            moverElemento(apoieElements.cmpCep);
            escrever(apoieElements.cmpCep, dto.getCep());
        }
        if (dto.getEstado() != null) {
            esperarElemento(apoieElements.cmpSelect.get(3));
            moverElemento(apoieElements.cmpSelect.get(3));
            clicarBotao(apoieElements.cmpSelect.get(3));
            escreverEntrar(apoieElements.inputTxtEstado, dto.getEstado());
        }
        if (dto.getCidade() != null) {
            esperarElemento(apoieElements.cmpCidade);
            moverElemento(apoieElements.cmpCidade);
            escrever(apoieElements.cmpCidade, dto.getCidade());
        }
        if (dto.getNumero() != null ) {
            esperarElemento(apoieElements.cmpNumero);
            moverElemento(apoieElements.cmpNumero);
            escrever(apoieElements.cmpNumero, dto.getNumero());
        }
        if (dto.getEndereco() != null ) {
            esperarElemento(apoieElements.cmpEndereco);
            moverElemento(apoieElements.cmpEndereco);
            escrever(apoieElements.cmpEndereco, dto.getEndereco());
        }
        if (dto.getAdc() != null) {
            esperarElemento(apoieElements.cmpAdc);
            moverElemento(apoieElements.cmpAdc);
            escrever(apoieElements.cmpAdc, dto.getAdc());
        }
        if (dto.getBairro() != null) {
            esperarElemento(apoieElements.cmpBairro);
            moverElemento(apoieElements.cmpBairro);
            escrever(apoieElements.cmpBairro, dto.getBairro());
        }
        if (dto.getTelefone() != null) {
            esperarElemento(apoieElements.cmpTelefone);
            moverElemento(apoieElements.cmpTelefone);
            escrever(apoieElements.cmpTelefone, dto.getTelefone());
        }
        if (dto.getEmail() != null) {
            esperarElemento(apoieElements.cmpEmail);
            moverElemento(apoieElements.cmpEmail);
            limparCampo(apoieElements.cmpEmail);
            escrever(apoieElements.cmpEmail, dto.getEmail());
        }
        if (dto.getNotas() != null) {
            esperarElemento(apoieElements.cmpNotas);
            moverElemento(apoieElements.cmpNotas);
            escrever(apoieElements.cmpNotas, dto.getNotas());
        }

    }

    public String determinarGenero(String nome) {
        String nomeLowerCase = nome.toLowerCase();

        if (nomeLowerCase.endsWith("a") || nomeLowerCase.endsWith("s")) {
            return "feminino";
        }

        if (nomeLowerCase.endsWith("o") || nomeLowerCase.endsWith("r")) {
            return "masculino";
        }

        return "outro";
    }

    public void selecionarTipoPagamento(String tipoPagto) throws Exception {

        try {
            switch (tipoPagto) {
                case "cartao de credito":
                    driver.switchTo().frame(apoieElements.iframeElementCredCard);
                    moverElemento(apoieElements.cmpTxtCredCard);
                    escrever(apoieElements.cmpTxtCredCard, gerarDadosCredCard());
                    driver.switchTo().defaultContent();
                    Thread.sleep(1000);

                    driver.switchTo().frame(apoieElements.iframeElementDtaValid);
                    esperarElemento(apoieElements.cmpTxtDtaValid);
                    escrever(apoieElements.cmpTxtDtaValid, gerarDadosDtaValid());
                    driver.switchTo().defaultContent();
                    Thread.sleep(1000);

                    driver.switchTo().frame(apoieElements.iframeElementCodSeg);
                    esperarElemento(apoieElements.cmpTxtCodSeguro);
                    escrever(apoieElements.cmpTxtCodSeguro, gerarDadosCvv());
                    driver.switchTo().defaultContent();
                    Thread.sleep(1000);
                    break;

                default:
                    throw new IllegalArgumentException("Método de Pagamento não existente!");
            }
        } catch (NoSuchElementException e) {
            System.out.println("O Selenium não conseguiu encontrar o elemento com o XPath especificado.");
        } catch (ElementNotInteractableException e) {
            System.out.println("O Selenium encontrou o elemento, mas ele não está em um estado que permita interação.");
        } catch (NullPointerException e) {
            System.out.println("Objeto não foi inicializado corretamente.");
        } catch (TimeoutException e) {
            System.out.println("O Selenium não conseguiu encontrar o elemento desejado dentro do tempo especificado.");
        } catch (NoSuchFrameException e) {
            System.out.println("O Selenium não conseguiu localizar o iframe especificado.");
        } catch (WebDriverException e) {
            System.out.println("O frame no qual você estava operando foi removido ou alterado na página web enquanto o teste estava em execução.");
        }

    }

    public void selecionarEConcordarComOsTermosECondicoesDoSiteEClicarEmAssineAgora() {
        esperarElemento(apoieElements.inputAceitarTermos);
        clicarBotao(apoieElements.inputAceitarTermos);
        esperarElemento(apoieElements.btnAssinarAgora);
        clicarBotao(apoieElements.btnAssinarAgora);
    }

    public void validoMensagemDeErro(String msgErro) {

        switch (msgErro) {
            case "cartao nao aprovado":
                esperarElemento(apoieElements.msgErroCartaoNaoAprovado);
                Assert.assertTrue("Validando mensagem de erro por cartão de crédito não aprovado",
                        apoieElements.msgErroCartaoNaoAprovado.getText().trim().contains(variaveis.txtMsgErroCartaoNaoAprovado));
                break;
            case "cartao incompleto":
                esperarElemento(apoieElements.msgErroCartaoNaoAprovado);
                Assert.assertTrue("Validando mensagem de erro por número cartão de crédito incompleto",
                        apoieElements.msgErroCartaoIncompleto.getText().trim().contains(variaveis.txtMsgErroCartaoIncompleto));
                break;
            case "cpf invalido":
                esperarElemento(apoieElements.msgErroCartaoNaoAprovado);
                Assert.assertTrue("Validando mensagem de erro por CPF inválido",
                        apoieElements.msgErroCpfInvalido.getText().trim().contains(variaveis.txtMSgErroCpfInvalido));
                break;
            default:
                throw new IllegalArgumentException("Método de Pagamento não existente!");
        }

    }

}
