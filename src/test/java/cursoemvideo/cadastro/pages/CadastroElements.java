package cursoemvideo.cadastro.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class CadastroElements {

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-0']")
    public WebElement cmpNome;

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-1']")
    public WebElement cmpSobrenome;

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-2']")
    public WebElement cmpEmail;

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-20']")
    public WebElement cmpConEmail;

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-3']")
    public WebElement cmpSenha;

    @FindBy(xpath = "//input[@id='field-5ed1238febe3e-4']")
    public WebElement cmpConSenha;

    @FindBy(xpath = "//button[@class='pp-button pp-submit-button']")
    public WebElement btnCadastro;

    @FindBy(xpath = "//a[@id='cn-accept-cookie']")
    public WebElement btnAceitaCookie;

    @FindBy(xpath = "//span[@class='pp-rf-error-inline']")
    public WebElement msgErroEmailJaExist;

    @FindBy(xpath = "//div[@class='pp-rf-pws-status short']")
    public WebElement msgSenhaFraca;

    @FindBy(xpath = "//div[@class='pp-rf-pws-status good']")
    public WebElement msgSenhaMedia;

    @FindBy(xpath = "//div[@class='pp-rf-pws-status strong']")
    public WebElement msgSenhaForte;

    @FindBy(xpath = "//span[@class='pp-rf-error pp-rf-error-custom'][contains(text(), 'Os e-mails não correspondem.')]")
    public WebElement msgErroEmailDiver;

    @FindBy(xpath = "//span[@class='pp-rf-error pp-rf-error-custom'][contains(text(), 'Campo obrigatório.')]")
    public List<WebElement> msgCampoObrigatorio;

    @FindBy(xpath = "//span[@class='pp-rf-error-inline']")
    public WebElement msgEmailInvalido;

}
