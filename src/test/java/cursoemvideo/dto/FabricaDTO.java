package cursoemvideo.dto;

import io.cucumber.core.internal.com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;

public class FabricaDTO {

    public static LoginDTO LoginDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, LoginDTO.class);
    }

    public static CadastroDTO CadastroDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, CadastroDTO.class);
    }

    public static AlterarDadosDTO AlterarDadosContaDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, AlterarDadosDTO.class);
    }

    public static ApoieDTO ApoieDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, ApoieDTO.class);
    }

}
