package cursoemvideo.meupainel.steps;

import cursoemvideo.dto.ApoieDTO;
import cursoemvideo.dto.FabricaDTO;
import cursoemvideo.meupainel.pages.apoie.ApoiePage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class ApoieSteps {

    private ApoiePage apoiePage;

    @Before
    public void open() {
        apoiePage = new ApoiePage();
    }

    @And("^seleciona o campo Quero ser um Apoiador$")
    public void selecionarCampoQueroSerApoiador() throws Exception {
        apoiePage.selecionarCampoQueroSerApoiador();
    }

    @And("^escolher apoio (.*)$")
    public void escolherApoio(String tipoApoio) throws Exception {
        apoiePage.escolherApoio(tipoApoio);
    }

    @And("^clica em Assine Agora$")
    public void clicaEmAssineAgora() throws Exception {
        apoiePage.clicaEmAssineAgora();
    }

    @And("^selecionar o tipo de pagamento (.*)$")
    public void selecionarTipoPagamento(String tipoPagto) throws Exception {
        apoiePage.selecionarTipoPagamento(tipoPagto);
    }

    @When("^na tela Finalizar compra preenche os campos$")
    public void preencherCamposFinalizarCompra(List<Map<String, String>> map) throws Exception {
        ApoieDTO dto = FabricaDTO.ApoieDTO(map.get(0));
        apoiePage.preencherCamposFinalizarCompraPessoaFisica(dto);
    }

    @When("^na tela Finalizar compra preenche os campos de Detalhes de faturamento de forma automatica$")
    public void preencherCamposFinalizarCompraAutomatico() throws Exception {
        ApoieDTO apoieDTO = apoiePage.gerarDadosApoiePessoaFisica();
        apoiePage.preencherCamposFinalizarCompraPessoaFisicaAutomatico(apoieDTO);
    }

    @And("^selecionar e concordar com os termos e condicoes do site e clicar em assine agora$")
    public void selecionarEConcordarComOsTermosECondicoesDoSiteEClicarEmAssineAgora() {
        apoiePage.selecionarEConcordarComOsTermosECondicoesDoSiteEClicarEmAssineAgora();
    }

    @Then("^valido mensagem de erro (.*)$")
    public void validoMensagemDeErro(String msgErro) {
        apoiePage.validoMensagemDeErro(msgErro);
    }

}
