package cursoemvideo.meupainel.pages.apoie;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ApoieElements {

    @FindBy(xpath = "(//div[@class='uabb-infobox-left-right-wrap']//a[@class='uabb-infobox-module-link'])[6]")
    public WebElement cmpAssinatura;

    @FindBy(xpath = "//a[@class='pp-button']")
    public List<WebElement> btnQueroSerApoiador;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/produto/aluno-apoiador-mensal/']")
    public WebElement btnApoieAgoraMensal;

    @FindBy(xpath = "//a[@href='https://www.cursoemvideo.com/produto/aluno-apoiador-anual/']")
    public WebElement btnApoieAgoraAnual;

    @FindBy(xpath = "//button[@name='add-to-cart']")
    public WebElement btnAssineAgora;

    @FindBy(xpath = "//input[@id='billing_first_name']")
    public WebElement cmpNome;

    @FindBy(xpath = "//input[@id='billing_last_name']")
    public WebElement cmpSobrenome;

    @FindBy(xpath = "//input[@class='select2-search__field']")
    public WebElement inputTxtTipoPessoa;

    @FindBy(xpath = "//span[@class='select2-selection select2-selection--single']")
    public List<WebElement> cmpSelect;

    @FindBy(xpath = "//input[@class='select2-search__field' and @aria-owns='select2-billing_gender-results']")
    public WebElement inputTxtGenero;

    @FindBy(xpath = "//input[@class='select2-search__field' and @aria-owns='select2-billing_country-results']")
    public WebElement inputTxtPais;

    @FindBy(xpath = "//input[@class='select2-search__field' and @aria-owns='select2-billing_state-results']")
    public WebElement inputTxtEstado;

    @FindBy(xpath = "//input[@id='billing_cpf']")
    public WebElement cmpCpf;

    @FindBy(xpath = "//input[@id='billing_birthdate']")
    public WebElement cmpDataNasc;

    @FindBy(xpath = "//input[@id='billing_postcode']")
    public WebElement cmpCep;

    @FindBy(xpath = "//input[@id='billing_address_1']")
    public WebElement cmpEndereco;

    @FindBy(xpath = "//input[@id='billing_number']")
    public WebElement cmpNumero;

    @FindBy(xpath = "//input[@id='billing_address_2']")
    public WebElement cmpAdc;

    @FindBy(xpath = "//input[@id='billing_neighborhood']")
    public WebElement cmpBairro;

    @FindBy(xpath = "//input[@id='billing_city']")
    public WebElement cmpCidade;

    @FindBy(xpath = "//input[@id='billing_phone']")
    public WebElement cmpTelefone;

    @FindBy(xpath = "//input[@id='billing_email']")
    public WebElement cmpEmail;

    @FindBy(xpath = "//textarea[@id='order_comments']")
    public WebElement cmpNotas;

    @FindBy(xpath = "//input[@id='payment_method_paypal']")
    public WebElement selectPayPal;

    @FindBy(xpath = "//input[@id='payment_method_pagseguro_assinaturas']")
    public WebElement selectPagRecorrente;

    @FindBy(xpath = "//input[@class='InputElement is-empty Input Input--empty' and @aria-label='Número do cartão de crédito ou débito']")
    public WebElement cmpTxtCredCard;

    @FindBy(xpath = "//input[@class='InputElement is-empty Input Input--empty' and @aria-label='Data de validade do cartão de crédito ou débito']")
    public WebElement cmpTxtDtaValid;

    @FindBy(xpath = "//input[@class='InputElement is-empty Input Input--empty' and @aria-label='CVC/CVV do cartão de crédito ou débito']")
    public WebElement cmpTxtCodSeguro;

    @FindBy(xpath = "//iframe[@title='Quadro seguro de entrada do número do cartão']")
    public WebElement iframeElementCredCard;

    @FindBy(xpath = "//iframe[@title='Quadro seguro de entrada da data de validade']")
    public WebElement iframeElementDtaValid;

    @FindBy(xpath = "//iframe[@title='Quadro seguro de entrada do CVC']")
    public WebElement iframeElementCodSeg;

    @FindBy(xpath = "//input[@id='terms']")
    public WebElement inputAceitarTermos;

    @FindBy(xpath = "//button[@id='place_order']")
    public WebElement btnAssinarAgora;

    @FindBy(xpath = "//li[contains(text(), 'O seu cartão não foi aprovado.')]")
    public WebElement msgErroCartaoNaoAprovado;

    @FindBy(xpath = "//li[contains(text(), 'O cartão está incompleto.')]")
    public WebElement msgErroCartaoIncompleto;

    @FindBy(xpath = "//li[normalize-space()='CPF é inválido.']")
    public WebElement msgErroCpfInvalido;

}
