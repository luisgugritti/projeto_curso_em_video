package cursoemvideo.meupainel.pages.cursosexclusivos;

import cursoemvideo.base.BasePage;
import cursoemvideo.utils.Variaveis;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CursosExclusivosPage extends BasePage {

    protected WebDriver driver;
    private CursosExclusivosElements cursosExclusivosElements;
    private Variaveis variaveis;

    public CursosExclusivosPage() {
        this.driver = BasePage.getDriver();
        cursosExclusivosElements = new CursosExclusivosElements();
        PageFactory.initElements(driver, cursosExclusivosElements);
    }

    public void clicarOpcao(String tipoCurso) throws Exception {

        switch (tipoCurso) {
            case "[IMPORTANTE] Recados aos apoiadores":
                esperarElemento(cursosExclusivosElements.recadosImportantes.get(2));
                clicarBotao(cursosExclusivosElements.recadosImportantes.get(2));
                break;
            case "Como anunciar na Internet [VIP]":
                esperarElemento(cursosExclusivosElements.cursoComoAnunciarNaInternet.get(2));
                clicarBotao(cursosExclusivosElements.cursoComoAnunciarNaInternet.get(2));
                break;
            case "Segurança da Informação: Módulo 1 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoSegurancaInformacaoMod1.get(2));
                clicarBotao(cursosExclusivosElements.cursoSegurancaInformacaoMod1.get(2));
                break;
            case "Segurança da Informação: Módulo 2 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoSegurancaInformacaoMod2.get(2));
                clicarBotao(cursosExclusivosElements.cursoSegurancaInformacaoMod2.get(2));
                break;
            case "Segurança da Informação: Módulo 00 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoSegurancaInformacaoMod0.get(2));
                clicarBotao(cursosExclusivosElements.cursoSegurancaInformacaoMod0.get(2));
                break;
            case "Segurança da Informação: Módulo 4 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoSegurancaInformacaoMod4.get(2));
                clicarBotao(cursosExclusivosElements.cursoSegurancaInformacaoMod4.get(2));
                break;
            case "Adequando sites e lojas virtuais em WordPress à LGPD [VIP]":
                esperarElemento(cursosExclusivosElements.cursoWordpressMod4.get(2));
                clicarBotao(cursosExclusivosElements.cursoWordpressMod4.get(2));
                break;
            case "Curso de PHP Moderno Módulo 1 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoPhpModernoMod1.get(2));
                clicarBotao(cursosExclusivosElements.cursoPhpModernoMod1.get(2));
                break;
            case "Curso Prático de SEO: Módulo 01 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoPraticoCEOMod1.get(2));
                clicarBotao(cursosExclusivosElements.cursoPraticoCEOMod1.get(2));
                break;
            case "Curso Prático de SEO: Módulo 02 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoPraticoCEOMod2.get(2));
                clicarBotao(cursosExclusivosElements.cursoPraticoCEOMod2.get(2));
                break;
            case "Curso de Inglês para Iniciantes – Módulo 1: Beginners [VIP]":
                esperarElemento(cursosExclusivosElements.cursoInglesInicMod1Beg.get(2));
                clicarBotao(cursosExclusivosElements.cursoInglesInicMod1Beg.get(2));
                break;
            case "Curso de Inglês para Iniciantes – Módulo 2: Beginners [VIP]":
                esperarElemento(cursosExclusivosElements.cursoInglesInicMod2Beg.get(2));
                clicarBotao(cursosExclusivosElements.cursoInglesInicMod2Beg.get(2));
                break;
            case "Curso de Inglês para Iniciantes – Módulo 3: Beginners [VIP]":
                esperarElemento(cursosExclusivosElements.cursoInglesInicMod3Beg.get(2));
                clicarBotao(cursosExclusivosElements.cursoInglesInicMod3Beg.get(2));
                break;
            case "HTML5 e CSS3: Módulo 1 de 5 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoIHtmlCssMod1.get(2));
                clicarBotao(cursosExclusivosElements.cursoIHtmlCssMod1.get(2));
                break;
            case "HTML5 e CSS3: Módulo 2 de 5 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoIHtmlCssMod2.get(2));
                clicarBotao(cursosExclusivosElements.cursoIHtmlCssMod2.get(2));
                break;
            case "HTML5 e CSS3: Módulo 3 de 5 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoIHtmlCssMod3.get(2));
                clicarBotao(cursosExclusivosElements.cursoIHtmlCssMod3.get(2));
                break;
            case "HTML5 e CSS3: Módulo 4 de 5 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoIHtmlCssMod4.get(2));
                clicarBotao(cursosExclusivosElements.cursoIHtmlCssMod4.get(2));
                break;
            case "HTML5 e CSS3: Módulo 5 de 5 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoIHtmlCssMod5.get(2));
                clicarBotao(cursosExclusivosElements.cursoIHtmlCssMod5.get(2));
                break;
            case "Bastidores do CursoemVídeo [VIP]":
                esperarElemento(cursosExclusivosElements.bastidoresCursoEmVideo.get(2));
                clicarBotao(cursosExclusivosElements.bastidoresCursoEmVideo.get(2));
                break;
            case "Algoritmos com Portugol Studio [VIP]":
                esperarElemento(cursosExclusivosElements.algoritmosPortugolStudio.get(2));
                clicarBotao(cursosExclusivosElements.algoritmosPortugolStudio.get(2));
                break;
            case "Curso em Vídeo Experience":
                esperarElemento(cursosExclusivosElements.cursoEmVideoExperience.get(2));
                clicarBotao(cursosExclusivosElements.cursoEmVideoExperience.get(2));
                break;
            case "Segurança da Informação: Módulo 3 [VIP]":
                esperarElemento(cursosExclusivosElements.cursoSegurancaInformacaoMod3.get(2));
                clicarBotao(cursosExclusivosElements.cursoSegurancaInformacaoMod3.get(2));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void validarCursoComRestricao(String cenario) throws Exception {

        switch (cenario) {
            case "[IMPORTANTE] Recados aos apoiadores":
            case "Como anunciar na Internet [VIP]":
            case "Segurança da Informação: Módulo 1 [VIP]":
            case "Segurança da Informação: Módulo 2 [VIP]":
            case "Segurança da Informação: Módulo 00 [VIP]":
            case "Segurança da Informação: Módulo 4 [VIP]":
            case "Adequando sites e lojas virtuais em WordPress à LGPD [VIP]":
            case "Curso de PHP Moderno Módulo 1 [VIP]":
            case "Curso Prático de SEO: Módulo 01 [VIP]":
            case "Curso Prático de SEO: Módulo 02 [VIP]":
            case "Curso de Inglês para Iniciantes – Módulo 1: Beginners [VIP]":
            case "Curso de Inglês para Iniciantes – Módulo 2: Beginners [VIP]":
            case "Curso de Inglês para Iniciantes – Módulo 3: Beginners [VIP]":
            case "HTML5 e CSS3: Módulo 1 de 5 [VIP]":
            case "HTML5 e CSS3: Módulo 2 de 5 [VIP]":
            case "HTML5 e CSS3: Módulo 3 de 5 [VIP]":
            case "HTML5 e CSS3: Módulo 4 de 5 [VIP]":
            case "HTML5 e CSS3: Módulo 5 de 5 [VIP]":
            case "Bastidores do CursoemVídeo [VIP]":
            case "Algoritmos com Portugol Studio [VIP]":
            case "Curso em Vídeo Experience":
            case "Segurança da Informação: Módulo 3 [VIP]":
                esperarElemento(cursosExclusivosElements.situacaoCurso);

                Assert.assertTrue("Validando a situação como exclusiva para assinantes",
                        cursosExclusivosElements.situacaoCurso.getText().contains("Fechado"));

                fechar();
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

}
