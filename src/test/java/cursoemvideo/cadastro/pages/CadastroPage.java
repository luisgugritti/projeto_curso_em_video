package cursoemvideo.cadastro.pages;

import cursoemvideo.dto.CadastroDTO;
import cursoemvideo.base.BasePage;
import cursoemvideo.utils.Urls;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CadastroPage extends BasePage {

    protected WebDriver driver;
    private CadastroElements cadastroElements;

    public CadastroPage() {
        this.driver = BasePage.getDriver();
        cadastroElements = new CadastroElements();
        PageFactory.initElements(driver, cadastroElements);
    }

    public void acessarTelaCadastro() throws Exception {
        acessarUrl(Urls.urlCadastro);
        esperarElemento(cadastroElements.btnAceitaCookie);
        clicarBotao(cadastroElements.btnAceitaCookie);
    }

    public void preencherCmpsCadastro(CadastroDTO dto) throws Exception {
        if (dto.getNome() != null) {
            esperarElemento(cadastroElements.cmpNome);
            escrever(cadastroElements.cmpNome, dto.getNome());
        }

        if (dto.getSobrenome() != null) {
            esperarElemento(cadastroElements.cmpSobrenome);
            escrever(cadastroElements.cmpSobrenome, dto.getSobrenome());
        }

        if (dto.getEmail() != null) {
            esperarElemento(cadastroElements.cmpEmail);
            escrever(cadastroElements.cmpEmail, dto.getEmail());
        }

        if (dto.getcEmail() != null) {
            esperarElemento(cadastroElements.cmpConEmail);
            escrever(cadastroElements.cmpConEmail, dto.getcEmail());
        }

        if (dto.getSenha() != null) {
            esperarElemento(cadastroElements.cmpSenha);
            escrever(cadastroElements.cmpSenha, dto.getSenha());
        }

        if (dto.getcSenha() != null) {
            esperarElemento(cadastroElements.cmpConSenha);
            escrever(cadastroElements.cmpConSenha, dto.getcSenha());
        }

    }

    public void validarCadastroRealizadoSucesso() throws Exception {
        esperarUrl(Urls.urlPagInicial);

        Assert.assertTrue("Validando cadastro com sucesso",
                Urls.urlPagInicial.contains("https://www.cursoemvideo.com/minha-conta/"));
    }

    public void validarMensagemDeErro(String tipo) throws Exception {

        switch (tipo) {
            case "inserir email ja existente":
                esperarElemento(cadastroElements.msgErroEmailJaExist);

                Assert.assertTrue("Validando erro ao inserir email ja existente",
                        cadastroElements.msgErroEmailJaExist.getText().trim().contains("The email is already registered, please choose another one."));
                break;
            case "inserir email divergente":
                esperarElemento(cadastroElements.msgErroEmailDiver);

                Assert.assertTrue("Validando erro ao inserir email divergente",
                        cadastroElements.msgErroEmailDiver.getText().trim().contains("Os e-mails não correspondem."));
                break;
            case "deixar os campos vazios":
                esperarElemento(cadastroElements.msgCampoObrigatorio.get(0));

                Assert.assertTrue("Validando erro ao deixar os campos vazios",
                        cadastroElements.msgCampoObrigatorio.get(0).getText().trim().contains("Campo obrigatório."));
                break;
            case "inserir email invalido":
                esperarElemento(cadastroElements.msgEmailInvalido);

                Assert.assertTrue("Validando erro ao inserir um email inválido",
                        cadastroElements.msgEmailInvalido.getText().trim().contains("The email address isn’t correct!"));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void validarMensagemPorSenha(String tipo) throws Exception {

        switch (tipo) {
            case "inserir uma senha fraca":
                esperarElemento(cadastroElements.msgSenhaFraca);

                Assert.assertTrue("Validando mensagem ao inserir senha fraca",
                        cadastroElements.msgSenhaFraca.getText().contains("Muito fraca"));
                break;
            case "inserir uma senha media":
                esperarElemento(cadastroElements.msgSenhaMedia);

                Assert.assertTrue("Validando mensagem ao inserir senha media",
                        cadastroElements.msgSenhaMedia.getText().contains("Médio"));
                break;
            case "inserir uma senha forte":
                esperarElemento(cadastroElements.msgSenhaForte);

                Assert.assertTrue("Validando mensagem ao inserir senha forte",
                        cadastroElements.msgSenhaForte.getText().contains("Forte"));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void clicarBtnCadastrar() throws Exception {
        esperarElemento(cadastroElements.btnCadastro);
        clicarBotao(cadastroElements.btnCadastro);
    }

    public void preencherCmpsCadastroAutomatico() throws Exception {
        CadastroDTO dto = geradorDeDados();

        if (dto.getNome() != null) {
            esperarElemento(cadastroElements.cmpNome);
            escrever(cadastroElements.cmpNome, dto.getNome());
        }

        if (dto.getSobrenome() != null) {
            esperarElemento(cadastroElements.cmpSobrenome);
            escrever(cadastroElements.cmpSobrenome, dto.getSobrenome());
        }

        if (dto.getEmail() != null) {
            esperarElemento(cadastroElements.cmpEmail);
            escrever(cadastroElements.cmpEmail, dto.getEmail());
        }

        if (dto.getcEmail() != null) {
            esperarElemento(cadastroElements.cmpConEmail);
            escrever(cadastroElements.cmpConEmail, dto.getcEmail());
        }

        if (dto.getSenha() != null) {
            esperarElemento(cadastroElements.cmpSenha);
            escrever(cadastroElements.cmpSenha, dto.getSenha());
        }

        if (dto.getcSenha() != null) {
            esperarElemento(cadastroElements.cmpConSenha);
            escrever(cadastroElements.cmpConSenha, dto.getcSenha());
        }

    }

}