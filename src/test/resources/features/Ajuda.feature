@Ajuda
  Feature: Tela de Ajuda

    Background: Visualizar e enviar perguntas e dúvidas sobre o portal
      Given acessar tela de login
      When realizar login
        | login                 | senha       |
        | arnaldo1234@gmail.com | arnaldo4213 |

      @CT001
      Scenario: Visualizar resposta da Dúvida sobre Pagamento pix com status pendente
        When seleciona a opcao Ajuda
        And seleciono a duvida Pagamento pix com status pendente
        Then verifico se o conteudo do campo Pagamento pix com status pendente esta correto
        And seleciono a duvida Certificados
        Then verifico se o conteudo do campo Certificados esta correto
        And seleciono a duvida Gerar Certificados
        Then verifico se o conteudo do campo Gerar Certificados esta correto
        And seleciono a duvida Certificado enviado
        Then verifico se o conteudo do campo Certificado enviado esta correto
        And seleciono a duvida Emitir Certificado
        Then verifico se o conteudo do campo Emitir Certificado esta correto
        And seleciono a duvida Conteudo Curso
        Then verifico se o conteudo do campo Conteudo Curso esta correto
        And seleciono a duvida Certificados Outra Pessoa
        Then verifico se o conteudo do campo Certificados Outra Pessoa esta correto
        And seleciono a duvida Cancelar Certificado
        Then verifico se o conteudo do campo Cancelar Certificado esta correto
        And seleciono a duvida Credito nao disponivel
        Then verifico se o conteudo do campo Credito nao disponivel esta correto
        And seleciono a duvida Credito nao aprovado
        Then verifico se o conteudo do campo Credito nao aprovado esta correto
        And seleciono a duvida Refazer curso
        Then verifico se o conteudo do campo Refazer curso esta correto
        And seleciono a duvida Certificado antigo
        Then verifico se o conteudo do campo Certificado antigo esta correto

      @CT002
      Scenario: Validar envio de mensagem sobre assunto Pagamento não processado
        When seleciona a opcao Ajuda
        And seleciono o assunto Pagamento nao processado
        And preencho o campo obrigatorio Mensagem
        And clico no botao Enviar
        Then valida mensagem de sucesso

      @CT003
      Scenario: Validar envio de mensagem sobre assunto Material do Curso
        When seleciona a opcao Ajuda
        And seleciono o assunto Material do Curso
        And preencho o campo obrigatorio Mensagem
        And clico no botao Enviar
        Then valida mensagem de sucesso

      @CT004
      Scenario: Validar envio de mensagem sobre assunto Problema no Progresso do Curso
        When seleciona a opcao Ajuda
        And seleciono o assunto Problema no Progresso do Curso
        And preencho o campo obrigatorio Mensagem
        And clico no botao Enviar
        Then valida mensagem de sucesso

      @CT005
      Scenario: Validar envio de mensagem sobre assunto Sugestao, Critica ou Elogio
        When seleciona a opcao Ajuda
        And seleciono o assunto Sugestao, Critica ou Elogio
        And preencho o campo obrigatorio Mensagem
        And clico no botao Enviar
        Then valida mensagem de sucesso

      @CT006
      Scenario: Validar envio de mensagem sobre assunto Outro
        When seleciona a opcao Ajuda
        And seleciono o assunto Outro
        And preencho o campo obrigatorio Mensagem
        And clico no botao Enviar
        Then valida mensagem de sucesso

      @CT007
      Scenario: Validar mensagem de erro ao deixar o campo obrigatório Mensagem vazio
        When seleciona a opcao Ajuda
        And clico no botao Enviar
        Then valida mensagem de erro