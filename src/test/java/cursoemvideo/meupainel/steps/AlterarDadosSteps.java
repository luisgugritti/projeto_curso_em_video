package cursoemvideo.meupainel.steps;

import cursoemvideo.dto.AlterarDadosDTO;
import cursoemvideo.dto.FabricaDTO;
import cursoemvideo.meupainel.pages.alterardados.AlterarDadosPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class AlterarDadosSteps {

    private AlterarDadosPage alterarDadosPage;

    @Before
    public void open() {
        alterarDadosPage = new AlterarDadosPage();
    }

    @When("^seleciona a opcao (.*)$")
    public void selecionaOpcao(String opcao) throws Exception {
        alterarDadosPage.selecionaOpcao(opcao);
    }

    @And("^alterar os campos$")
    public void alterarCampos(List<Map<String, String>> mapa) throws Exception {
        AlterarDadosDTO dto = FabricaDTO.AlterarDadosContaDTO(mapa.get(0));
        alterarDadosPage.alterarCampos(dto);
    }

    @And("^clicar no botao Salvar Alteracoes$")
    public void clicarBtnSalvarAlterar() throws Exception {
        alterarDadosPage.clicarBtnSalvarAlterar();
    }

    @Then("^validar mensagem (.*)$")
    public void validarAcaoNaTelaDeAlteracao(String acao) throws Exception {
        alterarDadosPage.validarAcaoNaTelaDeAlteracao(acao);
    }

    @And("^apaga o conteudo do campo (.*)$")
    public void apagaConteudoCampo(String campo) throws Exception {
        alterarDadosPage.apagaConteudoCampo(campo);
    }

    @Then("^validar se os campos voltaram aos valores iniciais$")
    public void validaConteudoCampo() throws Exception {
        alterarDadosPage.validaConteudoCampo();
    }

    @Then("^validar senha com nivel (.*)$")
    public void validarNivelSenha(String nivel) throws Exception {
        alterarDadosPage.validarNivelSenha(nivel);
    }

}
