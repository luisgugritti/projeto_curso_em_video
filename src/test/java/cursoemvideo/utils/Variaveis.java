package cursoemvideo.utils;

public class Variaveis {

    public String conteudoCmpPixTxt = "O pagamento através de PIX depende de uma confirmação que é enviada para a plataforma MercadoPago, este processamento demora em média 2 horas.";
    public String conteudoCampCertifTxt = "Você pode corrigir o seu nome no certificado acessando o menu “Cadastro”";
    public String conteudoGerarCertfTxt = "A emissão do certificado depende da conclusão do curso e aprovação no teste final com 70% de acertos";
    public String conteudoCertifEnv = "Não enviamos certificados pelo correio o certificado é digital, e você pode imprimir conforme seu desejo.";
    public String conteudoEmitirCertif = "A emissão de certificados é um serviço pago.";
    public String conteudoCursoTxt = "O Curso em Vídeo não presta suporte relacionados ao conteúdo dos cursos gratuitos disponíveis na plataforma. Você pode obter ajuda no nosso grupo do";
    public String conteudoCertificaOtrPessoa = "Não. Você só pode emitir certificados em seu nome. Cada usuário deve ter seu próprio cadastro.";
    public String conteudoCancCertif = "Não. Uma vez emitido um certificado o crédito será debitado e o certificado não pode ser cancelado.";
    public String conteudoCredNaoDisp = "O pagamento através de boleto bancário depende da compensação bancário e pode levar até 2 dias úteis para o crédito ser disponibilizado.";
    public String conteudoIndispPagtoCred = "Os pagamentos são processados pelo PagSeguro. Se seu cartão não está sendo aprovado entre em contato com sua operado de cartão.";
    public String conteudoRefazerCurso = "Pelo Youtube não temos como validar se o curso foi concluído, a plataforma antiga foi descontinuada por isso para emissão de certificados é necessário concluir o curso pela plataforma.";
    public String conteudoCertifAntigo = "A plataforma antiga estava com diversos problemas e se tornou inviável de manter os certificados sem custos.";
    public String txtMsg = "Teste Automatizado.";
    public String txtMsgSucesso = "Mensagem enviada com sucesso! Em breve entraremos em contato. Você receberá uma resposta em até 2 dias úteis.";
    public String txtMsgErro = "Este campo é obrigatório.";
    public String txtMsgErroCartaoNaoAprovado = "O seu cartão não foi aprovado.";
    public String txtMsgErroCartaoIncompleto = "O cartão está incompleto.";
    public String txtMSgErroCpfInvalido = "CPF é inválido.";

}
