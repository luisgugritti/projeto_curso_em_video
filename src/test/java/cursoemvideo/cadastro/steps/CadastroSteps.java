package cursoemvideo.cadastro.steps;

import cursoemvideo.cadastro.pages.CadastroPage;
import cursoemvideo.dto.CadastroDTO;
import cursoemvideo.dto.FabricaDTO;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class CadastroSteps {

    private CadastroPage cadastroPage;

    @Before
    public void before() {
        cadastroPage = new CadastroPage();
    }

    @Given("^acessar tela de cadastro$")
    public void acessarTelaCadastro() throws Exception {
        cadastroPage.acessarTelaCadastro();
    }

    @When("^preencho os campos na tela de cadastro$")
    public void preencherCmpsCadastro(List<Map<String, String>> mapa) throws Exception {
        CadastroDTO dto = FabricaDTO.CadastroDTO(mapa.get(0));
        cadastroPage.preencherCmpsCadastro(dto);
    }

    @Then("^validar cadastro realizado com sucesso$")
    public void validarCadastroRealizadoSucesso() throws Exception {
        cadastroPage.validarCadastroRealizadoSucesso();
    }

    @Then("^validar mensagem de erro ao (.*)$")
    public void validarMensagemDeErro(String tipo) throws Exception {
        cadastroPage.validarMensagemDeErro(tipo);
    }

    @Then("^validar mensagem ao (.*)$")
    public void validarMensagemPorSenha(String tipo) throws Exception {
        cadastroPage.validarMensagemPorSenha(tipo);
    }

    @And("^clicar no botao Registrar Gratuitamente$")
    public void clicarBtnCadastrar() throws Exception {
        cadastroPage.clicarBtnCadastrar();
    }

    @When("^preencho os campos de cadastro de forma automatica$")
    public void preencherCmpsCadastroAutomatico() throws Exception {
        cadastroPage.preencherCmpsCadastroAutomatico();
    }

}
