package cursoemvideo.meupainel.steps;

import cursoemvideo.meupainel.pages.cursosexclusivos.CursosExclusivosPage;
import cursoemvideo.base.BasePage;
import cursoemvideo.login.pages.LoginPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;

public class CursosExclusivosSteps extends BasePage {

    private WebDriver driver;
    private LoginPage loginPage;
    private CursosExclusivosPage cursosExclusivosPage;

    @Before
    public void open() {
        driver = BasePage.getDriver();
        loginPage = new LoginPage();
        cursosExclusivosPage = new CursosExclusivosPage();
    }

    @And("clico na opcao {string}")
    public void clicarOpcao(String tipoCurso) throws Exception {
        cursosExclusivosPage.clicarOpcao(tipoCurso);
    }

    @Then("validar que o curso {string} esta restringido a assinantes")
    public void validarCursoComRestricao(String cenario) throws Exception {
        cursosExclusivosPage.validarCursoComRestricao(cenario);
    }

}
