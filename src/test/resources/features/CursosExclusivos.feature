@CursosExclusivos
  Feature: Tela de Cursos Exclusivos

    Background: Realizar verificação se os cursos estão exclusivos
      Given acessar tela de login
      When realizar login
        | login                 | senha       |
        | arnaldo1234@gmail.com | arnaldo4213 |

      @CT001
        Scenario Outline: Validar que os cursos exclusivos estão restritos a assintantes - PT1
          When seleciona a opcao Cursos Exclusivos
          And clico na opcao <tipoCurso>
          Then validar que o curso <cenario> esta restringido a assinantes

          Examples:
            | cenario                                    | tipoCurso                                  |
            | "[IMPORTANTE] Recados aos apoiadores"      | "[IMPORTANTE] Recados aos apoiadores"      |
            | "Segurança da Informação: Módulo 00 [VIP]" | "Segurança da Informação: Módulo 00 [VIP]" |
            | "Segurança da Informação: Módulo 1 [VIP]"  | "Segurança da Informação: Módulo 1 [VIP]"  |
            | "Segurança da Informação: Módulo 2 [VIP]"  | "Segurança da Informação: Módulo 2 [VIP]"  |
            | "Segurança da Informação: Módulo 3 [VIP]"  | "Segurança da Informação: Módulo 3 [VIP]"  |
            | "Segurança da Informação: Módulo 4 [VIP]"  | "Segurança da Informação: Módulo 4 [VIP]"  |

      @CT002
      Scenario Outline: Validar que os cursos exclusivos estão restritos a assintantes - PT2
        When seleciona a opcao Cursos Exclusivos
        And clico na opcao <tipoCurso>
        Then validar que o curso <cenario> esta restringido a assinantes

        Examples:
          | cenario                                                       | tipoCurso                                                     |
          | "Adequando sites e lojas virtuais em WordPress à LGPD [VIP]"  | "Adequando sites e lojas virtuais em WordPress à LGPD [VIP]"  |
          | "Curso de PHP Moderno Módulo 1 [VIP]"                         | "Curso de PHP Moderno Módulo 1 [VIP]"                         |
          | "Curso Prático de SEO: Módulo 01 [VIP]"                       | "Curso Prático de SEO: Módulo 01 [VIP]"                       |
          | "Curso Prático de SEO: Módulo 02 [VIP]"                       | "Curso Prático de SEO: Módulo 02 [VIP]"                       |
          | "Curso de Inglês para Iniciantes – Módulo 1: Beginners [VIP]" | "Curso de Inglês para Iniciantes – Módulo 1: Beginners [VIP]" |
          | "Curso de Inglês para Iniciantes – Módulo 2: Beginners [VIP]" | "Curso de Inglês para Iniciantes – Módulo 2: Beginners [VIP]" |

      @CT003
      Scenario Outline: Validar que os cursos exclusivos estão restritos a assintantes - PT2
        When seleciona a opcao Cursos Exclusivos
        And clico na opcao <tipoCurso>
        Then validar que o curso <cenario> esta restringido a assinantes

        Examples:
          | cenario                                                       | tipoCurso                                                     |
          | "Curso de Inglês para Iniciantes – Módulo 3: Beginners [VIP]" | "Curso de Inglês para Iniciantes – Módulo 3: Beginners [VIP]" |
          | "Bastidores do CursoemVídeo [VIP]"                            | "Bastidores do CursoemVídeo [VIP]"                            |
          | "Algoritmos com Portugol Studio [VIP]"                        | "Algoritmos com Portugol Studio [VIP]"                        |
          | "HTML5 e CSS3: Módulo 1 de 5 [VIP]"                           | "HTML5 e CSS3: Módulo 1 de 5 [VIP]"                           |
          | "HTML5 e CSS3: Módulo 2 de 5 [VIP]"                           | "HTML5 e CSS3: Módulo 2 de 5 [VIP]"                           |
          | "HTML5 e CSS3: Módulo 3 de 5 [VIP]"                           | "HTML5 e CSS3: Módulo 3 de 5 [VIP]"                           |

      @CT004
      Scenario Outline: Validar que os cursos exclusivos estão restritos a assintantes - PT2
        When seleciona a opcao Cursos Exclusivos
        And clico na opcao <tipoCurso>
        Then validar que o curso <cenario> esta restringido a assinantes

        Examples:
          | cenario                             | tipoCurso                           |
          | "HTML5 e CSS3: Módulo 5 de 5 [VIP]" | "HTML5 e CSS3: Módulo 5 de 5 [VIP]" |
          | "HTML5 e CSS3: Módulo 4 de 5 [VIP]" | "HTML5 e CSS3: Módulo 4 de 5 [VIP]" |
          | "Curso em Vídeo Experience"         | "Curso em Vídeo Experience"         |
          | "Como anunciar na Internet [VIP]"   | "Como anunciar na Internet [VIP]"   |