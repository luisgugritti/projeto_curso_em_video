@AlterarDados
  Feature: Tela de alteração de dados da conta

    Background: Realizar alteração dos dados dados da conta
      Given acessar tela de login
      When realizar login
        | login                 | senha       |
        | arnaldo1234@gmail.com | arnaldo4213 |

      @CT001
      Scenario: Realizar alteração de senha
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual   | novaSenha    | cNovaSenha   |
          | arnaldo4213  | 42136arnaldo | 42136arnaldo |
        And clicar no botao Salvar Alteracoes
        Then validar mensagem senha alterada com sucesso
        
      @CT002
      Scenario: Senha atual incorreta
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual  | novaSenha    | cNovaSenha   |
          | arnaldo4333 | 42136arnaldo | 42136arnaldo |
        And clicar no botao Salvar Alteracoes
        Then validar mensagem senha atual incorreta

      @CT003
      Scenario: Novas senhas divergentes uma da outra
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual   | novaSenha    | cNovaSenha |
          | arnaldo4213  | 42136arnaldo | 4213arnl   |
        And clicar no botao Salvar Alteracoes
        Then validar mensagem senhas novas divergentes

      @CT004
      Scenario: Senha não alterada pois os campos estão vazios
        When seleciona a opcao Cadastro
        And clicar no botao Salvar Alteracoes
        Then validar mensagem quando os campos nao sao preenchidos

      @CT005
      Scenario: Mensagem de erro quando os campos obrigatórios estão vazios
        When seleciona a opcao Cadastro
        And apaga o conteudo do campo nome, sobrenome, exibicao e email
        And clicar no botao Salvar Alteracoes
        Then validar mensagem quando os campos obrigatorios estao vazios

      @CT006
      Scenario: Mensagem de erro por passar um email inválido
        When seleciona a opcao Cadastro
        And alterar os campos
          | email             | senhaAtual  | novaSenha      | cNovaSenha     |
          | arnaldo1234@gmail | arnaldo4213 | arnaldo4213564 | arnaldo4213564 |
        And clicar no botao Salvar Alteracoes
        Then validar mensagem por passar um email invalido

      @CT007
      Scenario: Validar quando uma ação é realizada o conteúdo dos campos obrigatórios voltam tendo o mesmo valor
        When seleciona a opcao Cadastro
        And alterar os campos
          | nome | sobrenome | nomeExibicao | email             | senhaAtual  | novaSenha    | cNovaSenha   |
          | Arln | Coelho    | Arnld        | arnaldo1234@gmail | arnaldo4213 | arnaldo12354 | arnaldo12354 |
        And clicar no botao Salvar Alteracoes
        Then validar se os campos voltaram aos valores iniciais

      @CT008
      Scenario: Validar quando o usuário define uma senha muito fraca
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual  | novaSenha | cNovaSenha |
          | arnaldo4213 | 421       | 421        |
        Then validar senha com nivel muito fraca

      @CT009
      Scenario: Validar quando o usuário define uma senha fraca
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual  | novaSenha | cNovaSenha |
          | arnaldo4213 | 421@arnl  | 421@arnl   |
        Then validar senha com nivel fraca

      @CT010
      Scenario: Validar quando o usuário define uma senha média
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual  | novaSenha    | cNovaSenha   |
          | arnaldo4213 | 42136@Arnald | 42136@Arnald |
        Then validar senha com nivel medio

      @CT011
      Scenario: Validar quando o usuário define uma senha forte
        When seleciona a opcao Cadastro
        And alterar os campos
          | senhaAtual  | novaSenha          | cNovaSenha         |
          | arnaldo4213 | 4213678978952@Arna | 4213678978952@Arna |
        Then validar senha com nivel forte