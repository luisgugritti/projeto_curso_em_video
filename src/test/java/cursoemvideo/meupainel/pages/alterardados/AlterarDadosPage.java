package cursoemvideo.meupainel.pages.alterardados;

import cursoemvideo.dto.AlterarDadosDTO;
import cursoemvideo.meupainel.pages.ajuda.AjudaElements;
import cursoemvideo.base.BasePage;
import cursoemvideo.meupainel.pages.apoie.ApoieElements;
import cursoemvideo.meupainel.pages.cursosexclusivos.CursosExclusivosElements;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AlterarDadosPage extends BasePage {

    protected WebDriver driver;
    private AlterarDadosElements alterarDadosElements;
    private AjudaElements ajudaElements;
    private CursosExclusivosElements cursosExclusivosElements;
    private ApoieElements apoieElements;

    public AlterarDadosPage() {
        this.driver = BasePage.getDriver();
        alterarDadosElements = new AlterarDadosElements();
        ajudaElements = new AjudaElements();
        apoieElements = new ApoieElements();
        cursosExclusivosElements = new CursosExclusivosElements();
        PageFactory.initElements(driver, apoieElements);
        PageFactory.initElements(driver, cursosExclusivosElements);
        PageFactory.initElements(driver, alterarDadosElements);
        PageFactory.initElements(driver, ajudaElements);
    }

    public void selecionaOpcao(String opcao) throws Exception {

        switch (opcao) {
            case "Cadastro":
                esperarElemento(alterarDadosElements.cmpCadastro);
                moverElemento(alterarDadosElements.cmpCadastro);
                clicarBotao(alterarDadosElements.cmpCadastro);
                break;
            case "Ajuda":
                esperarElemento(ajudaElements.cmpAjuda);
                moverElemento(ajudaElements.cmpAjuda);
                clicarBotao(ajudaElements.cmpAjuda);
                break;
            case "Cursos Exclusivos":
                esperarElemento(cursosExclusivosElements.cmpCursosExclusivos);
                moverElemento(cursosExclusivosElements.cmpCursosExclusivos);
                clicarBotao(cursosExclusivosElements.cmpCursosExclusivos);
                break;
            case "Assinatura":
                esperarElemento(apoieElements.cmpAssinatura);
                moverElemento(apoieElements.cmpAssinatura);
                clicarBotao(apoieElements.cmpAssinatura);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void alterarCampos(AlterarDadosDTO dto) throws Exception {

        if (dto.getNome() != null) {
            esperarElemento(alterarDadosElements.cmpNome);
            limparCampo(alterarDadosElements.cmpNome);
            escrever(alterarDadosElements.cmpNome, dto.getNome());
        }

        if (dto.getSobrenome() != null) {
            esperarElemento(alterarDadosElements.cmpSobrenome);
            limparCampo(alterarDadosElements.cmpSobrenome);
            escrever(alterarDadosElements.cmpSobrenome, dto.getSobrenome());
        }

        if (dto.getNomeExibicao() != null) {
            esperarElemento(alterarDadosElements.cmpNomeExibicao);
            limparCampo(alterarDadosElements.cmpNomeExibicao);
            escrever(alterarDadosElements.cmpNomeExibicao, dto.getNomeExibicao());
        }

        if (dto.getEmail() != null) {
            esperarElemento(alterarDadosElements.cmpEmail);
            limparCampo(alterarDadosElements.cmpEmail);
            escrever(alterarDadosElements.cmpEmail, dto.getEmail());
        }

        if (dto.getSenhaAtual() != null) {
            esperarElemento(alterarDadosElements.cmpSenhaAtual);
            escrever(alterarDadosElements.cmpSenhaAtual, dto.getSenhaAtual());
        }

        if (dto.getNovaSenha() != null) {
            esperarElemento(alterarDadosElements.cmpNovaSenha);
            escrever(alterarDadosElements.cmpNovaSenha, dto.getNovaSenha());
        }

        if (dto.getcNovaSenha() != null) {
            esperarElemento(alterarDadosElements.cmpConfNovaSenha);
            escrever(alterarDadosElements.cmpConfNovaSenha, dto.getcNovaSenha());
        }

    }

    public void clicarBtnSalvarAlterar() throws Exception {
        esperarElemento(alterarDadosElements.btnSalvarAlteracoes);
        clicarBotao(alterarDadosElements.btnSalvarAlteracoes);
    }

    public void validarAcaoNaTelaDeAlteracao(String acao) throws Exception {

        if (acao.equals("senha alterada com sucesso") || acao.equals("quando os campos nao sao preenchidos")) {
            esperarElemento(alterarDadosElements.msgRetornoAlteracao);

            Assert.assertTrue("Validando alteração de senha realizada com sucesso",
                    alterarDadosElements.msgRetornoAlteracao.getText().trim().contains("Detalhes da conta modificados com sucesso."));
        }

        if (acao.equals("senha atual incorreta")) {
            esperarElemento(alterarDadosElements.msgSenhaIncorreta);

            Assert.assertTrue("Validando mensagem de erro ao inserir a senha atual de forma incorreta",
                    alterarDadosElements.msgSenhaIncorreta.getText().trim().contains("Sua senha atual está incorreta."));
        }

        if (acao.equals("senhas novas divergentes")) {
            esperarElemento(alterarDadosElements.msgSenhaIncorreta);

            Assert.assertTrue("Validando mensagem de erro ao inserir as novas senhas de forma divergente",
                    alterarDadosElements.msgSenhaIncorreta.getText().trim().contains("As novas senhas não são iguais."));
        }

        if (acao.equals("quando os campos obrigatorios estao vazios")) {
            esperarElemento(alterarDadosElements.msgSenhaIncorreta);

            Assert.assertTrue("Validando mensagem de erro ao deixar os campos obrigatórios vazios",
                    alterarDadosElements.msgSenhaIncorreta.getText().trim().contains("é um campo obrigatório"));
        }

        if (acao.equals("por passar um email invalido")) {
            esperarElemento(alterarDadosElements.msgSenhaIncorreta);

            Assert.assertTrue("Validando mensagem de erro ao passar um email inválido",
                    alterarDadosElements.msgSenhaIncorreta.getText().trim().contains("Informe um endereço de e-mail válido."));
        }

    }

    public void apagaConteudoCampo(String campo) throws Exception {

        switch (campo){
            case "nome, sobrenome, exibicao e email":
                esperarElemento(alterarDadosElements.cmpNome);
                limparCampo(alterarDadosElements.cmpNome);
                limparCampo(alterarDadosElements.cmpSobrenome);
                limparCampo(alterarDadosElements.cmpNomeExibicao);
                limparCampo(alterarDadosElements.cmpEmail);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

        public String salvarNome() {
            return salvarConteudoCampo(alterarDadosElements.cmpNome);
        }

        public String salvarSobrenome() {
            return salvarConteudoCampo(alterarDadosElements.cmpSobrenome);
        }

        public String salvarNomeExibicao() {
            return salvarConteudoCampo(alterarDadosElements.cmpNomeExibicao);
        }

        public String salvarEmail() {
            return salvarConteudoCampo(alterarDadosElements.cmpEmail);
        }


    public void validaConteudoCampo() throws Exception {
        String nomeInicial = salvarNome();
        String sobrenomeInicial = salvarSobrenome();
        String nomeExibicaoInicial = salvarNomeExibicao();
        String emailInicial = salvarEmail();

        Assert.assertEquals("Validando se o valor original permanece no campo Nome",
                nomeInicial, salvarNome());
        Assert.assertEquals("Validando se o valor original permanece no campo Sobrenome",
                sobrenomeInicial, salvarSobrenome());
        Assert.assertEquals("Validando se o valor original permanece no campo Nome Exibição",
                nomeExibicaoInicial, salvarNomeExibicao());
        Assert.assertEquals("Validando se o valor original permanece no campo Email",
                emailInicial, salvarEmail());
    }

    public void validarNivelSenha(String nivel) throws Exception {

        switch (nivel) {
            case "muito fraca":
                esperarElemento(alterarDadosElements.txtNivelSenha);

                Assert.assertTrue("Validando se o nível da senha é muito fraca",
                        alterarDadosElements.txtNivelSenha.getText().trim().contains("Muito fraca - Digite uma senha segura."));
                break;
            case "fraca":
                esperarElemento(alterarDadosElements.txtNivelSenha);

                Assert.assertTrue("Validando se o nível da senha é fraca",
                        alterarDadosElements.txtNivelSenha.getText().trim().contains("Fraca - Digite uma senha segura."));
                break;
            case "medio":
                esperarElemento(alterarDadosElements.txtNivelSenha);

                Assert.assertTrue("Validando se o nível da senha é médio",
                        alterarDadosElements.txtNivelSenha.getText().trim().contains("Médio"));
                break;
            case "forte":
                esperarElemento(alterarDadosElements.txtNivelSenha);

                Assert.assertTrue("Validando se o nível da senha é forte",
                        alterarDadosElements.txtNivelSenha.getText().trim().contains("Forte"));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }
}