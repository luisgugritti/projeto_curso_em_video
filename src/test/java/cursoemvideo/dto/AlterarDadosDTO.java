package cursoemvideo.dto;

public class AlterarDadosDTO {

    private String nome;
    private String sobrenome;
    private String nomeExibicao;
    private String email;
    private String senhaAtual;
    private String novaSenha;
    private String cNovaSenha;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getNomeExibicao() {
        return nomeExibicao;
    }

    public void setNomeExibicao(String nomeExibicao) {
        this.nomeExibicao = nomeExibicao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getcNovaSenha() {
        return cNovaSenha;
    }

    public void setcNovaSenha(String cNovaSenha) {
        this.cNovaSenha = cNovaSenha;
    }

}
