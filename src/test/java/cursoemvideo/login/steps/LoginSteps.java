package cursoemvideo.login.steps;

import cursoemvideo.dto.FabricaDTO;
import cursoemvideo.dto.LoginDTO;
import cursoemvideo.login.pages.LoginPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class LoginSteps {

    private LoginPage loginPage;

    @Before
    public void before() {
        loginPage = new LoginPage();
    }

    @Given("^acessar tela de login$")
    public void acessarTelaDeLogin() throws Exception {
        loginPage.acessarTelaDeLogin();
    }

    @Given("^acessar tela de login sem cookie$")
    public void acessarTelaDeLoginSemCookie() throws Exception {
        loginPage.acessarTelaDeLoginSemCookie();
    }

    @When("^realizar login$")
    public void realizarLogin(List<Map<String, String>> mapa) throws Exception {
        LoginDTO dto = FabricaDTO.LoginDTO(mapa.get(0));
        loginPage.realizarLogin(dto);
    }

    @Then("^validar login realizado com sucesso$")
    public void validarLoginDeSucesso() throws Exception {
        loginPage.validarLoginDeSucesso();
    }

    @Then("^validação de alerta de erro sobre senha incorreta$")
    public void validarMsgErroSenhaIncorreta() throws Exception {
        loginPage.validarMsgErroSenhaIncorreta();
    }

    @Then("^validar alerta de erro por email invalido$")
    public void validarMsgErroEmailInvalido() throws Exception {
        loginPage.validarMsgErroEmailInvalido();
    }

    @Given("^realizar login com os dados do cadastro$")
    public void facoLoginComOsDadosDoCadastro() {
        loginPage.facoLoginComOsDadosDoCadastro();
    }

}