package cursoemvideo.meupainel.pages.ajuda;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class AjudaElements {

    @FindBy(xpath = "(//div[@class='uabb-infobox-left-right-wrap']//a[@class='uabb-infobox-module-link'])[5]")
    public WebElement cmpAjuda;

    @FindBy(xpath = "//div[@class='uabb-adv-accordion-button uabb-adv-accordion-button5eaef2f766c71 uabb-adv-before-text']")
    public List<WebElement> tipoDuvida;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='O pagamento através de PIX depende de uma confirmação que é enviada para a plataforma MercadoPago, este processamento demora em média 2 horas.']")
    public WebElement respContPix;

    @FindBy(xpath = "//span[@style='font-weight: 400;']")
    public List<WebElement> restContCertificado;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='A emissão do certificado depende da conclusão do curso e aprovação no teste final com 70% de acertos']")
    public WebElement respContGerarCertif;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='Não enviamos certificados pelo correio o certificado é digital, e você pode imprimir conforme seu desejo.']")
    public WebElement respContCertifEnv;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//strong[text()='A emissão de certificados é um serviço pago.']")
    public WebElement respContEmitirCertif;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='O Curso em Vídeo não presta suporte relacionados ao conteúdo dos cursos gratuitos disponíveis na plataforma. Você pode obter ajuda no nosso grupo do ']")
    public WebElement respContCurso;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='Não. Você só pode emitir certificados em seu nome. Cada usuário deve ter seu próprio cadastro.']")
    public WebElement respContCertfOtrPessoa;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='Não. Uma vez emitido um certificado o crédito será debitado e o certificado não pode ser cancelado.']")
    public WebElement respContCancCertif;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='O pagamento através de boleto bancário depende da compensação bancário e pode levar até 2 dias úteis para o crédito ser disponibilizado.']")
    public WebElement respContCredNaoDisp;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='Os pagamentos são processados pelo PagSeguro. Se seu cartão não está sendo aprovado entre em contato com sua operado de cartão.']")
    public WebElement respContIndispPagtoCred;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='Pelo Youtube não temos como validar se o curso foi concluído, a plataforma antiga foi descontinuada por isso para emissão de certificados é necessário concluir o curso pela plataforma.']")
    public WebElement respContRefazerCurso;

    @FindBy(xpath = "//div[contains(@class, 'uabb-adv-accordion-content')]//p[text()='A plataforma antiga estava com diversos problemas e se tornou inviável de manter os certificados sem custos.']")
    public WebElement respContCertifAntigo;

    @FindBy(xpath = "//select[@id='input_2_6']")
    public WebElement cmpAssunto;

    @FindBy(xpath = "//input[@id='input_2_1']")
    public WebElement cmpTxtOutro;

    @FindBy(xpath = "//textarea[@id='input_2_5']")
    public WebElement cmpTxtMsg;

    @FindBy(xpath = "//input[@id='gform_submit_button_2']")
    public WebElement btnEnviar;

    @FindBy(xpath = "//div[@id='gform_confirmation_message_2']")
    public List<WebElement> msgSucesso;

    @FindBy(xpath = "//div[@id='validation_message_2_5']")
    public List<WebElement> msgErro;

}
