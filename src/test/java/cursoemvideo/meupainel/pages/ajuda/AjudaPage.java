package cursoemvideo.meupainel.pages.ajuda;

import cursoemvideo.base.BasePage;
import cursoemvideo.utils.Variaveis;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AjudaPage extends BasePage {

    protected WebDriver driver;
    private AjudaElements ajudaElements;
    private Variaveis variaveis;

    public AjudaPage() {
        this.driver = BasePage.getDriver();
        ajudaElements = new AjudaElements();
        variaveis = new Variaveis();
        PageFactory.initElements(driver, ajudaElements);
    }

    public void selecionarDuvida(String duvida) throws Exception {

        switch (duvida) {
            case "Pagamento pix com status pendente":
                esperarElemento(ajudaElements.tipoDuvida.get(0));
                moverElemento(ajudaElements.tipoDuvida.get(0));
                selecionar(ajudaElements.tipoDuvida.get(0));
                break;
            case "Certificados":
                esperarElemento(ajudaElements.tipoDuvida.get(1));
                moverElemento(ajudaElements.tipoDuvida.get(1));
                selecionar(ajudaElements.tipoDuvida.get(1));
                break;
            case "Gerar Certificados":
                esperarElemento(ajudaElements.tipoDuvida.get(2));
                moverElemento(ajudaElements.tipoDuvida.get(2));
                selecionar(ajudaElements.tipoDuvida.get(2));
                break;
            case "Certificado enviado":
                esperarElemento(ajudaElements.tipoDuvida.get(3));
                moverElemento(ajudaElements.tipoDuvida.get(3));
                selecionar(ajudaElements.tipoDuvida.get(3));
                break;
            case "Emitir Certificado":
                esperarElemento(ajudaElements.tipoDuvida.get(4));
                moverElemento(ajudaElements.tipoDuvida.get(4));
                selecionar(ajudaElements.tipoDuvida.get(4));
                break;
            case "Conteudo Curso":
                esperarElemento(ajudaElements.tipoDuvida.get(5));
                moverElemento(ajudaElements.tipoDuvida.get(5));
                selecionar(ajudaElements.tipoDuvida.get(5));
                break;
            case "Certificados Outra Pessoa":
                esperarElemento(ajudaElements.tipoDuvida.get(6));
                moverElemento(ajudaElements.tipoDuvida.get(6));
                selecionar(ajudaElements.tipoDuvida.get(6));
                break;
            case "Cancelar Certificado":
                esperarElemento(ajudaElements.tipoDuvida.get(7));
                moverElemento(ajudaElements.tipoDuvida.get(7));
                selecionar(ajudaElements.tipoDuvida.get(7));
                break;
            case "Credito nao disponivel":
                esperarElemento(ajudaElements.tipoDuvida.get(8));
                moverElemento(ajudaElements.tipoDuvida.get(8));
                selecionar(ajudaElements.tipoDuvida.get(8));
                break;
            case "Credito nao aprovado":
                esperarElemento(ajudaElements.tipoDuvida.get(9));
                moverElemento(ajudaElements.tipoDuvida.get(9));
                selecionar(ajudaElements.tipoDuvida.get(9));
                break;
            case "Refazer curso":
                esperarElemento(ajudaElements.tipoDuvida.get(10));
                moverElemento(ajudaElements.tipoDuvida.get(10));
                selecionar(ajudaElements.tipoDuvida.get(10));
                break;
            case "Certificado antigo":
                esperarElemento(ajudaElements.tipoDuvida.get(11));
                moverElemento(ajudaElements.tipoDuvida.get(11));
                selecionar(ajudaElements.tipoDuvida.get(11));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void verificarContCmp(String campo) throws Exception {

        switch (campo) {
            case "Pagamento pix com status pendente":
                esperarElemento(ajudaElements.respContPix);

                Assert.assertTrue("Validando conteúdo da dúvida Pagamento pix com status pendente",
                        ajudaElements.respContPix.getText().trim().contains(variaveis.conteudoCmpPixTxt));
                break;
            case "Certificados":
                esperarElemento(ajudaElements.restContCertificado.get(0));

                Assert.assertTrue("Validando conteúdo da dúvida sobre Certificados",
                        ajudaElements.restContCertificado.get(0).getText().trim().contains(variaveis.conteudoCampCertifTxt));
                break;
            case "Gerar Certificados":
                esperarElemento(ajudaElements.respContGerarCertif);

                Assert.assertTrue("Validando conteúdo da dúvida sobre Gerar Certificados",
                        ajudaElements.respContGerarCertif.getText().trim().contains(variaveis.conteudoGerarCertfTxt));
                break;
            case "Certificado enviado":
                esperarElemento(ajudaElements.respContCertifEnv);

                Assert.assertTrue("Validando conteúdo da dúvida sobre se o certificado é enviado pelo correio",
                        ajudaElements.respContCertifEnv.getText().trim().contains(variaveis.conteudoCertifEnv));
                break;
            case "Emitir Certificado":
                esperarElemento(ajudaElements.respContEmitirCertif);

                Assert.assertTrue("Validando conteúdo da dúvida sobre a emissão do certificado",
                        ajudaElements.respContEmitirCertif.getText().trim().contains(variaveis.conteudoEmitirCertif));
                break;
            case "Conteudo Curso":
                esperarElemento(ajudaElements.respContCurso);

                Assert.assertTrue("Validando conteúdo da dúvida sobre o conteúdo do curso",
                        ajudaElements.respContCurso.getText().trim().contains(variaveis.conteudoCursoTxt));
                break;
            case "Certificados Outra Pessoa":
                esperarElemento(ajudaElements.respContCertfOtrPessoa);

                Assert.assertTrue("Validando conteúdo da dúvida sobre o certificados de outras pessoas",
                        ajudaElements.respContCertfOtrPessoa.getText().trim().contains(variaveis.conteudoCertificaOtrPessoa));
                break;
            case "Cancelar Certificado":
                esperarElemento(ajudaElements.respContCancCertif);

                Assert.assertTrue("Validando conteúdo da dúvida sobre cancelamento de certificado",
                        ajudaElements.respContCancCertif.getText().trim().contains(variaveis.conteudoCancCertif));
                break;
            case "Credito nao disponivel":
                esperarElemento(ajudaElements.respContCredNaoDisp);

                Assert.assertTrue("Validando conteúdo da dúvida sobre crédito não disponível",
                        ajudaElements.respContCredNaoDisp.getText().trim().contains(variaveis.conteudoCredNaoDisp));
                break;
            case "Credito nao aprovado":
                esperarElemento(ajudaElements.respContIndispPagtoCred);

                Assert.assertTrue("Validando conteúdo da dúvida sobre crédito não aprovado",
                        ajudaElements.respContIndispPagtoCred.getText().trim().contains(variaveis.conteudoIndispPagtoCred));
                break;
            case "Refazer curso":
                esperarElemento(ajudaElements.respContRefazerCurso);

                Assert.assertTrue("Validando conteúdo da dúvida sobre ter que refazer o curso",
                        ajudaElements.respContRefazerCurso.getText().trim().contains(variaveis.conteudoRefazerCurso));
                break;
            case "Certificado antigo":
                esperarElemento(ajudaElements.respContCertifAntigo);

                Assert.assertTrue("Validando conteúdo da dúvida sobre certificados antigos",
                        ajudaElements.respContCertifAntigo.getText().trim().contains(variaveis.conteudoCertifAntigo));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }


    public void selecionarAssunto(String assunto) throws Exception {

        switch (assunto) {
            case "Pagamento nao processado":
                esperarElemento(ajudaElements.cmpAssunto);
                moverElemento(ajudaElements.cmpAssunto);
                selecionarOpcaoPorTexto(ajudaElements.cmpAssunto, "Pagamento não processado");
                break;
            case "Material do Curso":
                esperarElemento(ajudaElements.cmpAssunto);
                moverElemento(ajudaElements.cmpAssunto);
                selecionarOpcaoPorTexto(ajudaElements.cmpAssunto, "Material do curso");
                break;
            case "Problema no Progresso do Curso":
                esperarElemento(ajudaElements.cmpAssunto);
                moverElemento(ajudaElements.cmpAssunto);
                selecionarOpcaoPorTexto(ajudaElements.cmpAssunto, "Problemas para progressão dos cursos");
                break;
            case "Sugestao, Critica ou Elogio":
                esperarElemento(ajudaElements.cmpAssunto);
                moverElemento(ajudaElements.cmpAssunto);
                selecionarOpcaoPorTexto(ajudaElements.cmpAssunto, "Sugestão, crítica ou elogio");
                break;
            case "Outro":
                esperarElemento(ajudaElements.cmpAssunto);
                moverElemento(ajudaElements.cmpAssunto);
                selecionarOpcaoPorTexto(ajudaElements.cmpAssunto, "Outro");
                esperarElemento(ajudaElements.cmpTxtOutro);
                escrever(ajudaElements.cmpTxtOutro, "Teste");
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void preencherCmpMsg() throws Exception {
        esperarElemento(ajudaElements.cmpTxtMsg);
        moverElemento(ajudaElements.cmpTxtMsg);
        escrever(ajudaElements.cmpTxtMsg, variaveis.txtMsg);
    }

    public void clicarBtnEnviar() throws Exception {
        esperarElemento(ajudaElements.btnEnviar);
        moverElemento(ajudaElements.btnEnviar);
        clicarBotao(ajudaElements.btnEnviar);
    }

    public void validarTipoMsg(String msg) throws Exception {

        switch (msg) {
            case "sucesso":
                esperarElemento(ajudaElements.msgSucesso.get(0));

                Assert.assertTrue("Validando mensagem de sucesso ao enviar dúvida",
                        ajudaElements.msgSucesso.get(0).getText().trim().contains(variaveis.txtMsgSucesso));
                break;
            case "erro":
                esperarElemento(ajudaElements.msgErro.get(0));

                Assert.assertTrue("Validando mensagem de erro ao enviar dúvida",
                        ajudaElements.msgErro.get(0).getText().trim().contains(variaveis.txtMsgErro));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

}
