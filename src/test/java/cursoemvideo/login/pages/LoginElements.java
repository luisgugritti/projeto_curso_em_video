package cursoemvideo.login.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginElements {

    @FindBy(xpath = "//input[@name='uabb-lf-name']")
    public WebElement cmpEmail;

    @FindBy(xpath = "//input[@id='uabb-password-field']")
    public WebElement cmpSenha;

    @FindBy(xpath = "//button[@name='uabb-lf-login-submit']")
    public WebElement btnLogin;

    @FindBy(xpath = "//div[@class='uabb-lf-error-message-wrap']")
    public WebElement msgErroSenhaIncorreta;

    @FindBy(xpath = "//div[@class='uabb-lf-error-message-wrap']")
    public WebElement msgErroEmailInvalido;

    @FindBy(xpath = "//a[@id='cn-accept-cookie']")
    public WebElement btnAceitaCookie;

}
