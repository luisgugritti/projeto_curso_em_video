@Login
  Feature: Tela Login

    Background: Realizar Login no site Cusro em Vídeo

      @CT001
      Scenario: Realizar Login com sucesso
        Given acessar tela de cadastro
        When preencho os campos de cadastro de forma automatica
        And clicar no botao Registrar Gratuitamente
        When acessar tela de login sem cookie
        Then realizar login com os dados do cadastro

      @CT002
      Scenario: Senha incorreta
        Given acessar tela de login
        When realizar login
          | login                 | senha |
          | arnaldo1234@gmail.com | 42136 |
        Then validação de alerta de erro sobre senha incorreta

      @CT003
      Scenario: Email invalido
        Given acessar tela de login
        When realizar login
          | login             | senha        |
          | arnaldo1234@gmail | 42136arnaldo |
        Then validar alerta de erro por email invalido