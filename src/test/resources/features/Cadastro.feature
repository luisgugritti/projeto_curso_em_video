@Cadastro
  Feature: Tela de Cadastro

    Background: Realizar Cadastro de usuário
      Given acessar tela de cadastro

      @CT001
      Scenario: Cadastro realizado com sucesso
        When preencho os campos de cadastro de forma automatica
        And clicar no botao Registrar Gratuitamente
        Then validar cadastro realizado com sucesso

      @CT002
      Scenario: Email ja existente na base
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email                 | cEmail                | senha       | cSenha      |
          | Arnaldo | Mendes    | arnaldo1234@gmail.com | arnaldo1234@gmail.com | arnaldo4213 | arnaldo4213 |
        And clicar no botao Registrar Gratuitamente
        Then validar mensagem de erro ao inserir email ja existente

      @CT003
      Scenario: Senha muito fraca
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email                 | cEmail                | senha | cSenha |
          | Arnaldo | Mendes    | arnaldo12xz@gmail.com | arnaldo12xz@gmail.com | 13    | 13     |
        Then validar mensagem ao inserir uma senha fraca

      @CT004
      Scenario: Senha de nível medio
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email                | cEmail               | senha         | cSenha        |
          | Arnaldo | Mendes    | arnaldo123@gmail.com | arnaldo123@gmail.com | 421361234luis | 421361234luis |
        Then validar mensagem ao inserir uma senha media

      @CT005
      Scenario: Senha de nível forte
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email                | cEmail               | senha              | cSenha             |
          | Arnaldo | Mendes    | arnaldo123@gmail.com | arnaldo123@gmail.com | 4213678978952@Luis | 4213678978952@Luis |
        Then validar mensagem ao inserir uma senha forte

      @CT006
      Scenario: Emails divergentes
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email                | cEmail              | senha         | cSenha        |
          | Arnaldo | Mendes    | arnaldo123@gmail.com | arnaldo12@gmail.com | 421361234luis | 421361234luis |
        Then validar mensagem de erro ao inserir email divergente

      @CT007
      Scenario: Email inválido
        When preencho os campos na tela de cadastro
          | nome    | sobrenome | email            | cEmail           | senha              | cSenha             |
          | Arnaldo | Mendes    | arnaldo123@gmail | arnaldo123@gmail | 4213678978952@Luis | 4213678978952@Luis |
        And clicar no botao Registrar Gratuitamente
        Then validar mensagem de erro ao inserir email invalido

      @CT008
      Scenario: Campos não preenchidos
        When clicar no botao Registrar Gratuitamente
        Then validar mensagem de erro ao deixar os campos vazios
