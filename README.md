
# **Projeto Curso Em Video**

## 📝 Descrição
Este projeto é uma aplicação Maven baseada em Java que utiliza Selenium, JUnit e Cucumber para testes automatizados seguindo a arquitetura PageObjects. É um exemplo prático desenvolvido por mim para prática diária.

## 🛠️ Tecnologias Utilizadas
- **Java 11**
- **Maven**
- **Selenium**
- **Cucumber**
- **JUnit**
- **SLF4J**
- **Java Faker**

## 📋 Requisitos de Instalação
- **JDK 11** ou superior
- **Maven**
- **Navegador Google Chrome** (para testes com Selenium)

## 🚀 Instruções de Instalação

1. **Clone o repositório:**
   ```bash
   git clone https://github.com/seu-usuario/Projeto_Curso_Em_Video.git
   cd Projeto_Curso_Em_Video
   
Instale as dependências do Maven:

**mvn clean install**

## 📦 Como Usar

### Para executar os testes automatizados, use o comando:

**mvn test**

Os testes são configurados para serem executados com o plugin `maven-surefire-plugin` e incluem todos os arquivos que seguem o padrão `**/*Runner.java`.

Estrutura do Projeto

- `src/main/java`: Código fonte da aplicação.
- `src/test/java`: Código fonte dos testes.
- `pom.xml`: Arquivo de configuração do Maven.

## 🤝 Contribuição
1. Faça um fork do projeto.
2. Crie uma nova branch: `git checkout -b minha-branch`
3. Faça suas alterações e commit: `git commit -m 'Minha contribuição'`
4. Faça um push para a branch: `git push origin minha-branch`
5. Abra um Pull Request.

## ✨ Autores e Créditos

- **Luis Gustavo** - *Desenvolvedor principal*

`   **Agradecimentos a plataforma do Curso em Vídeo que inspirou este projeto.**
`