package cursoemvideo.base;

import com.github.javafaker.Faker;
import cursoemvideo.dto.ApoieDTO;
import cursoemvideo.dto.CadastroDTO;
import cursoemvideo.login.pages.LoginElements;
import cursoemvideo.meupainel.pages.apoie.ApoieElements;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class BasePage extends LoginElements {

    private static WebDriver driver;
    private static Faker faker = new Faker();
    private static CadastroDTO cadastroDTO;
    ApoieElements apoieElements = new ApoieElements();

    public static WebDriver getDriver() {
        if (driver == null) {
            startDriver();
        }
        return driver;
    }

    private static void startDriver() {

        try {
            ChromeOptions chromeOpts = new ChromeOptions();
            chromeOpts.addArguments("--lang=pt-br");
            chromeOpts.addArguments("start-maximized");
            chromeOpts.addArguments("--remote-allow-origins=*");
            chromeOpts.addArguments("ignore-certificate-errors");
            chromeOpts.addArguments("--no-sandbox");
            chromeOpts.addArguments("load-extension=C:/automacao/extensoes/uBlock");
            System.setProperty("webdriver.chrome.driver", "C:/automacao/webdriver/chromedriver.exe");
            driver = new ChromeDriver(chromeOpts);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Falha ao iniciar o ChromeDriver: " + e.getMessage());
        }

    }

    public static void acessarUrl(String url) {
        getDriver().get(url);
    }

    public void clicarBotao(WebElement btn) {
        if (btn != null) {
            btn.click();
        } else {
            throw new IllegalArgumentException("Botão não pode ser nulo");
        }
    }

    public void clicarLink(WebElement link) {
        if (link != null) {
            link.click();
        } else {
            throw new IllegalArgumentException("Link não pode ser nulo");
        }
    }

    public void escrever(WebElement elemento, String write) {
        if (elemento != null && write != null) {
            elemento.sendKeys(write);
        } else {
            throw new IllegalArgumentException("Elemento e texto a ser escrito não podem ser nulos");
        }
    }

    public void escreverEntrar(WebElement elemento, String write) {
        if (elemento != null && write != null) {
            elemento.sendKeys(write);
            elemento.sendKeys(Keys.ENTER);
        } else {
            throw new IllegalArgumentException("Elemento e texto a ser escrito não podem ser nulos");
        }
    }

    public void limparCampo(WebElement clean) {
        if (clean != null) {
            clean.clear();
        } else {
            throw new IllegalArgumentException("Campo a ser limpo não pode ser nulo");
        }
    }

    public static void fechar() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public void esperarUrl(String url) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.urlToBe(url));
    }

    public void esperarElemento(WebElement waitElement) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.visibilityOf(waitElement));
    }

    public void esperarElementos(List<WebElement> waitElements) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.visibilityOfAllElements(waitElements));
    }

    public void moverElemento(WebElement el) {
        JavascriptExecutor js = (JavascriptExecutor) this.driver;

        try {
            js.executeScript("arguments[0].scrollIntoView();", new Object[]{el});
        } catch (Exception var4) {
            System.out.println("Elemento não encontrado.");
        }
    }

    public String salvarConteudoCampo(WebElement campo) {
        if (campo != null) {
            return campo.getText();
        } else {
            throw new IllegalArgumentException("O campo não pode ser nulo");
        }
    }

    public void selecionar(WebElement elemento) {
        if (elemento != null && elemento.isDisplayed() && elemento.isEnabled()) {
            boolean estaAberto = elemento.getAttribute("aria-expanded").equals("true");
            if (!estaAberto) {
                elemento.click();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void selecionarOpcaoPorTexto(WebElement element, String texto) {
        if (!element.getTagName().equals("select")) {
            throw new IllegalArgumentException("Elemento deve ser um <select>");
        }
        Select select = new Select(element);
        select.selectByVisibleText(texto);
    }

    public void selecionarOpcaoPorValor(WebElement element, String valor) {
        Select select = new Select(element);
        select.selectByValue(valor);
    }

    public void selecionarOpcao(WebElement elemento, String opcao) {
        if (elemento != null && opcao != null) {
            Select select = new Select(elemento);
            select.selectByVisibleText(opcao);

            Actions actions = new Actions(driver);
            actions.moveToElement(elemento).click().build().perform();
        } else {
            throw new IllegalArgumentException("Elemento e opção a ser selecionada não podem ser nulos");
        }
    }

    public static void killChromeDriver() {
        ProcessBuilder processBuilder = new ProcessBuilder("taskkill", "/F", "/IM", "chromedriver.exe");

        try {
            Process process = processBuilder.start();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CadastroDTO geradorDeDados() {
        Faker faker = new Faker();
        CadastroDTO userData = new CadastroDTO();
        userData.setNome(faker.name().firstName());
        userData.setSobrenome(faker.name().lastName());
        String email = faker.internet().emailAddress();
        userData.setEmail(email);
        userData.setcEmail(email);
        String password = faker.internet().password(8, 16);
        userData.setSenha(password);
        userData.setcSenha(password);

        setCadastroDTO(userData);
        return userData;
    }

    public static CadastroDTO getCadastroDTO() {
        return cadastroDTO;
    }

    public static void setCadastroDTO(CadastroDTO cadastroDTO) {
        BasePage.cadastroDTO = cadastroDTO;
    }

    public ApoieDTO gerarDadosApoiePessoaFisica() {
        Faker faker = new Faker();
        ApoieDTO apoieDTO = new ApoieDTO();

        apoieDTO.setTipoPessoa("Pessoa Fisíca");
        apoieDTO.setCpf(faker.number().digits(11));
        apoieDTO.setDataNasc(faker.date().birthday().toString());
        apoieDTO.setGenero(faker.demographic().sex());
        apoieDTO.setPais("Brasil");
        apoieDTO.setCep("01153000");
        apoieDTO.setEndereco(faker.address().streetAddress());
        apoieDTO.setNumero(faker.address().buildingNumber());
        apoieDTO.setAdc(faker.address().secondaryAddress());
        apoieDTO.setBairro(faker.address().cityName());
        apoieDTO.setCidade("São Paulo");
        apoieDTO.setEstado("São Paulo");
        apoieDTO.setTelefone(faker.phoneNumber().phoneNumber());
        apoieDTO.setEmail(faker.internet().emailAddress());
        apoieDTO.setNotas(faker.lorem().sentence());
        CadastroDTO cadastroDTO = getCadastroDTO();
        apoieDTO.setNome(cadastroDTO.getNome());
        apoieDTO.setSobrenome(cadastroDTO.getSobrenome());
        apoieDTO.setEmail(cadastroDTO.getEmail());

        return apoieDTO;
    }

    public String gerarDadosCredCard() {
        Faker faker = new Faker();
        return faker.finance().creditCard();
    }

    public String gerarDadosDtaValid() {
        Random random = new Random();
        LocalDate hoje = LocalDate.now();
        int anoValidade = hoje.getYear() + random.nextInt(5) + 1;
        int mesValidade = random.nextInt(12) + 1;
        return String.format("%02d/%d", mesValidade, anoValidade % 100);
    }

    public String gerarDadosCvv() {
        Random random = new Random();
        return String.format("%03d", random.nextInt(1000));
    }

}