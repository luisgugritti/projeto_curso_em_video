package cursoemvideo.meupainel.pages.alterardados;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AlterarDadosElements {

    @FindBy(xpath = "(//div[@class='uabb-infobox-left-right-wrap']//a[@class='uabb-infobox-module-link'])[10]")
    public WebElement cmpCadastro;

    @FindBy(xpath = "//input[@id='account_first_name']")
    public WebElement cmpNome;

    @FindBy(xpath = "//input[@id='account_last_name']")
    public WebElement cmpSobrenome;

    @FindBy(xpath = "//input[@id='account_display_name']")
    public WebElement cmpNomeExibicao;

    @FindBy(xpath = "//input[@id='account_email']")
    public WebElement cmpEmail;

    @FindBy(xpath = "//input[@id='password_current']")
    public WebElement cmpSenhaAtual;

    @FindBy(xpath = "//input[@id='password_1']")
    public WebElement cmpNovaSenha;

    @FindBy(xpath = "//input[@id='password_2']")
    public WebElement cmpConfNovaSenha;

    @FindBy(xpath = "//button[@name='save_account_details']")
    public WebElement btnSalvarAlteracoes;

    @FindBy(xpath = "//div[@class='woocommerce-message']")
    public WebElement msgRetornoAlteracao;

    @FindBy(xpath = "//ul[@class='woocommerce-error']")
    public WebElement msgSenhaIncorreta;

    @FindBy(xpath = "//div[@aria-live='polite']")
    public WebElement txtNivelSenha;

}
